package com.megaapps.einsteingameNoAdds.events;

/**
 * Created by lukasz on 11.05.17.
 */

public class ReturnToMainScreenEvent {

    private int score;
    private boolean win;

    public ReturnToMainScreenEvent(int score, boolean win) {
        this.score = score;
        this.win = win;
    }

    public int getScore() {
        return score;
    }

    public boolean isWin() {
        return win;
    }
}
