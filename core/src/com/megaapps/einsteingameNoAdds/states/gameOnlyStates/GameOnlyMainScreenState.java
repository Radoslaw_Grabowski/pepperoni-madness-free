package com.megaapps.einsteingameNoAdds.states.gameOnlyStates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.Game;
import com.megaapps.einsteingameNoAdds.GameStateManager;
import com.megaapps.einsteingameNoAdds.events.ReturnToMainScreenEvent;
import com.megaapps.einsteingameNoAdds.helpers.FontHelper;
import com.megaapps.einsteingameNoAdds.helpers.ImageResizeHelper;
import com.megaapps.einsteingameNoAdds.helpers.MusicHelper;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.State;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.GameScreenState;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.board.ElementHelper;
import com.squareup.otto.Subscribe;

import static com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_HEIGHT;
import static com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_WIDTH;
import static com.megaapps.einsteingameNoAdds.Game.bus;

/**
 * Created by Radosław Grabowski on 12/06/2017.
 */

public final class GameOnlyMainScreenState extends State
{
	private final float MENU_PADDING = GAME_SIZE_HEIGHT / 15;
	private final float MENU_BUTTON_PADDING = GAME_SIZE_HEIGHT / 100;
	Label yourScoreLabel, highScoreLabel;
	boolean win = false;
	private Stage stage;
	private InputProcessor backProcessor;
	private InputMultiplexer m_inputMultiplexer;
	//private TextButton m_newGameButton;
	private GameScreenState m_gameScreenState;
	private TextureAtlas pizzaAtlas;


	public GameOnlyMainScreenState( final GameStateManager gsm, NewAssetManager as,
									GameScreenState gameScreenState )
	{
		super( gsm, as );
		bus.register( this );
		m_gameScreenState = gameScreenState;
	}

	@Override
	public void initialize()
	{
		ElementHelper.shuffleTables();
		turnOnOfMusicMusic();
		stage = new Stage( new FitViewport( GAME_SIZE_WIDTH, GAME_SIZE_HEIGHT ) );
		loadMenu();
		Game.addsInterface.showBannerAds( true );
		isInitialized = true;
	}

	@Override
	public void loadAssets()
	{
		assetManager.loadAsync( Constants.FONT_XX_SMALL_PATH,													 BitmapFont.class );
		assetManager.loadAsync( Constants.FONT_PATH,                                                             BitmapFont.class );
		assetManager.loadAsync( Constants.GAME_LOGO_PATH,                                                        Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_ERASER_PATH,                                         Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_MODIFIER_BACKGROUND_PATH,                            Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BACKGROUND_PATH,                    Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BLACK_LABEL_PATH,                   Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BOTTOM_LABEL_PATH,                  Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_TRASH_CAN_PATH,                     Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_TORNADO_PATH,                                        Texture.class );
		assetManager.loadAsync( Constants.MINI_GAME_TUTORIAL_STATE_GAME_TUTORIAL_PATH,                           Texture.class );
		assetManager.loadAsync( Constants.PIZZA_GAME_TUTORIAL_TUT1_PATH,                                         Texture.class );
		assetManager.loadAsync( Constants.PIZZA_GAME_TUTORIAL_TUT2_PATH,                                         Texture.class );
		assetManager.loadAsync( Constants.PIZZA_GAME_TUTORIAL_TUT3_PATH,                                         Texture.class );

		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_ELEMENTS_ATLAS_PATH,                TextureAtlas.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_ELEMENTS_PIZZA_ELEMENTS_ATLAS_PATH, TextureAtlas.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_PIZZA_MENU_ATLAS_PATH,              TextureAtlas.class );

		assetManager.loadAsync( Constants.THEME_MUSIC_PATH,                                                      Music.class );

		assetManager.loadAsync( Constants.CLICK_SOUND_PATH,                                                      Sound.class );
		assetManager.loadAsync( Constants.REMOVE_PIZZA_PATH,                                                     Sound.class );
	}

	@Override
	public void pause( boolean pauseMusic )
	{
		if( MusicHelper.getThemeMusic().isPlaying() )
			MusicHelper.getThemeMusic().pause();
	}

	@Override
	public void update( float dt )
	{
		stage.act( dt );
	}

	@Override
	public void render( SpriteBatch sb )
	{
		Gdx.gl.glClearColor( Constants.TUTORIAL_CLEAR_COLORS.x, Constants.TUTORIAL_CLEAR_COLORS.y, Constants.TUTORIAL_CLEAR_COLORS.z, 1 );
		stage.draw();
	}

	@Override
	public void dispose()
	{
		stage.dispose();
		bus.unregister( this );
	}

	@Override
	public void resize( int width, int height )
	{
		stage.getViewport().update( width, height );
	}

	@Override
	public void resume()
	{
		if( !PreferencesHelper.getMusicMute() )
			MusicHelper.getThemeMusic().play();
		setInputs();
	}

	@Override
	public void setInputs()
	{
		Gdx.input.setCatchBackKey( true );
		backProcessor = new InputAdapter()
		{
			@Override
			public boolean keyDown( int keycode )
			{
				if( ( keycode == Input.Keys.BACK ) )
					Game.homeButtonInterface.homeButtonClicked();
				return true;
			}
		};
		m_inputMultiplexer = new InputMultiplexer( stage, backProcessor );
		Gdx.input.setInputProcessor( m_inputMultiplexer );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	@Subscribe
	public void returnToMainScreen( ReturnToMainScreenEvent event )
	{
		Gdx.app.log( "GameOnlyMainScreenState", "returnToMainScreen" );

		if( event.isWin() )
		{
			ElementHelper.shuffleTables();
			setYourScoreLabel( event.getScore() );
			setHighscoreLabel();
		}
		else
			setYourScoreLabel( event.getScore() );

		gsm.popDispose();
	}


	private void loadMenu()
	{
		pizzaAtlas = assetManager.get( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_PIZZA_MENU_ATLAS_PATH, TextureAtlas.class );

		//creating menu Table

		//adding logo
		Image menuLogo = new Image( pizzaAtlas.findRegion( "logo" ) );
		ImageResizeHelper.setHeightAndChangeWidthProportionally( menuLogo, GAME_SIZE_HEIGHT / 2.5f );
		menuLogo.setPosition( GAME_SIZE_WIDTH / 2 - menuLogo.getWidth() / 2, GAME_SIZE_HEIGHT / 2 );
		stage.addActor( menuLogo );


		//adding highScoreContainer
		Table highScoreContainer = new Table();
		Label.LabelStyle highScoreStyle = new Label.LabelStyle( FontHelper.getFont( Constants.FontType.X_SMALL ), Color.YELLOW );
		Label.LabelStyle titleHighScoreStyle = new Label.LabelStyle( FontHelper.getFont( Constants.FontType.X_SMALL ), Color.WHITE );
		Label titleLabel = new Label( "best", titleHighScoreStyle );
		highScoreLabel = new Label( "", highScoreStyle );
		setHighscoreLabel();

		highScoreContainer.add( titleLabel ).size( titleLabel.getWidth(), titleLabel.getHeight() ).align( Align.center ).row();
		highScoreContainer.add( highScoreLabel ).size( highScoreLabel.getWidth(), highScoreLabel.getHeight() ).align( Align.center );
		highScoreContainer.pack();

		highScoreContainer.setPosition( MENU_PADDING, menuLogo.getY() - ( highScoreContainer.getHeight() + 1.2f * MENU_PADDING ) );
		stage.addActor( highScoreContainer );

		//adding yourScore Container
		Table yourScore = new Table();
		Label.LabelStyle titleYourStyle = new Label.LabelStyle( FontHelper.getFont( Constants.FontType.MEDIUM ), Color.WHITE );
		Label titleYourScoreLabel = new Label( "your score", titleHighScoreStyle );
		yourScoreLabel = new Label( "0", titleYourStyle );
		yourScoreLabel.setAlignment( Align.center );
		yourScore.add( titleYourScoreLabel ).size( titleYourScoreLabel.getWidth(), titleYourScoreLabel.getHeight() ).align( Align.center ).row();
		yourScore.add( yourScoreLabel ).size( yourScoreLabel.getWidth(), yourScoreLabel.getHeight() ).align( Align.center );
		yourScore.pack();

		yourScore.setPosition( GAME_SIZE_WIDTH / 2 - yourScore.getWidth() / 2, highScoreContainer.getY() );
		stage.addActor( yourScore );


		TextButton.TextButtonStyle playButtonStyle = new TextButton.TextButtonStyle();

		playButtonStyle.down = ( new Image( pizzaAtlas.findRegion( "playPressed" ) ) ).getDrawable();
		playButtonStyle.up = ( new Image( pizzaAtlas.findRegion( "play" ) ) ).getDrawable();
		playButtonStyle.font = FontHelper.getFont( Constants.FontType.SMALL );

		TextButton play = new TextButton( "PLAY", playButtonStyle );
		play.addListener( new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MusicHelper.playClickSound();

				if( m_gameScreenState != null )
				{
					m_gameScreenState.invalidateGame();
					if( win )
						m_gameScreenState = new GameScreenState( gsm, assetManager );

					gsm.push( m_gameScreenState );
					gsm.invalidate();
				}
				else
					Gdx.app.log( "GameOnlyMainScreenState", "FATAL ERROR" );
			}
		} );

		float ratio = play.getHeight() / play.getWidth();

		play.setSize( GAME_SIZE_WIDTH * 0.6f, ratio * GAME_SIZE_WIDTH * 0.6f );

		final ImageButton soundButton = new ImageButton( new Image( pizzaAtlas.findRegion( "sound" ) ).getDrawable(),
														 new Image( pizzaAtlas.findRegion( "soundPressed" ) ).getDrawable(),
														 new Image( pizzaAtlas.findRegion( "soundChecked" ) ).getDrawable() );
		soundButton.setSize( ( play.getWidth() - MENU_BUTTON_PADDING ) / 2, play.getHeight() );

		if( PreferencesHelper.getSoundMute() )
			soundButton.setChecked( true );
		else
			soundButton.setChecked( false );

		soundButton.addListener( new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				boolean sound = PreferencesHelper.getSoundMute();
				if( sound )
					MusicHelper.getClickSound().play();
				PreferencesHelper.saveSoundMute( !sound );
				soundButton.setChecked( !sound );
			}
		} );

		final ImageButton musicButton = new ImageButton( new Image( pizzaAtlas.findRegion( "music" ) ).getDrawable(),
														 new Image( pizzaAtlas.findRegion( "musicPressed" ) ).getDrawable(),
														 new Image( pizzaAtlas.findRegion( "musicChecked" ) ).getDrawable() );

		musicButton.setSize( ( play.getWidth() - MENU_BUTTON_PADDING ) / 2, play.getHeight() );

		if( PreferencesHelper.getMusicMute() )
			musicButton.setChecked( true );
		else
			musicButton.setChecked( false );

		musicButton.addListener( new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				MusicHelper.playClickSound();

				boolean music = PreferencesHelper.getMusicMute();
				PreferencesHelper.saveMusicMute( !music );
				//	bus.post(new TurnOnOfMusicEvent(true, !music));
				turnOnOfMusicMusic();
				musicButton.setChecked( !music );
			}
		} );

		play.setPosition( GAME_SIZE_WIDTH / 2 - play.getWidth() / 2, highScoreContainer.getY() - ( play.getHeight() + MENU_PADDING ) );
		stage.addActor( play );

		soundButton.setPosition( play.getX(), play.getY() - ( MENU_BUTTON_PADDING + soundButton.getHeight() ) );
		stage.addActor( soundButton );

		musicButton.setPosition( soundButton.getX() + soundButton.getWidth() + MENU_BUTTON_PADDING, soundButton.getY() );
		stage.addActor( musicButton );
	}

	private void turnOnOfMusicMusic()
	{
		if( !PreferencesHelper.getMusicMute() )
		{
			MusicHelper.getThemeMusic().play();
		}
		else MusicHelper.getThemeMusic().pause();
	}


	public void setYourScoreLabel( int val )
	{
		String text = String.valueOf( val );
		yourScoreLabel.setText( text );
		yourScoreLabel.setAlignment( Align.center );
		yourScoreLabel.pack();
	}

	private void setHighscoreLabel()
	{
		highScoreLabel.setText( String.valueOf( PreferencesHelper.getHighScore()[0] ) );
		highScoreLabel.setAlignment( Align.center );
		highScoreLabel.pack();
	}

}