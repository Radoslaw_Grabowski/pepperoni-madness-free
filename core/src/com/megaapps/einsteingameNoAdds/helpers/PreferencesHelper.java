package com.megaapps.einsteingameNoAdds.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import java.util.Hashtable;


import static com.badlogic.gdx.net.HttpRequestBuilder.json;

/**
 * Created by k on 24.04.2017.
 */

public class PreferencesHelper {

    public static Preferences pref;
    //PIZZA GAME PREFERENCES
    private static final String FIRST_TIME_INSTRUCTION = "firstTimeInstruction";
    private static final String TRASH_CAN_MODIFIERS_COUNTER = "trashCanCounter";
    private static final String ERASER_MODIFICATORS_COUNTER = "eraserCounter";
    private static final String TORNADO_MODIFICATORS_COUNTER = "tornadoCounter";
    private static final String LAST_INTERSTITIAL_TIMESTAMP = "lastIntestitalTimestamp";
    private static final int TRASH_CAN_MODIFIERS_DEFAULT_VALUE = 5;
    private static final int ERASER_MODIFICATORS_DEFAULT_VALUE = 5;
    private static final int TORNADO_MODIFICATORS_DEFAULT_VALUE = 5;
    // /PIZZA GAME PREFERENCES
    private static final String EXP_POINTS = "expPoints";
    private static final String UPGRADE_POINTS = "upgradePoints";
    private static final String MAX_BOOK_COUNT = "maxBookCount";
    private static final String PER_BOOK_EXP = "perBookExp";
    private static final String LAST_TIMEOUT = "lastTimeout";
    public static final String PREFERENCES_NAME = "pepperoniMadnessFree";
    public static final String LEVEL = "level";
    public static final String LEVEL_PROGRESS = "levelProgress";
    public static final String BOOKS_AMOUNT = "booksAmount";
//    public static final String BOOKS_CHAPTER = "booksChapter";
    public static final String MONEY_AMOUNT = "money";
    public static final String HIGH_SCORE = "highscore";
    public static final String SOUND_MUTE = "soundMute";
    public static final String MUSIC_MUTE = "musicMute";
    private static final int DEFAULT_EXP = 0;
    private static final int DEFAULT_UPGRADE_POINTS = 0;
    private static final int DEFAULT_MAX_BOOK_COUNT = 1;
    private static final int DEFAULT_PER_BOOK_EXP = 200;
    public static final int DEFAULT_LEVEL = 1;
    public static final float DEFAULT_PROGRESS = 1f;
    public static final float DEFAULT_BOOKS_AMOUNT = 0f;
//    private static final int DEFAULT_CHAPTER = 0;
    private static final int DEFAULT_MONEY_VAL = 0;
    private static final boolean DEFAULT_MUTE = false;
    private static final boolean FIRST_TIME_DEFAULT_VALUE=true;


    public static Preferences getPreferences() {
        return Gdx.app.getPreferences(PREFERENCES_NAME);
    }

    public static void saveExp(float value){
        pref = getPreferences();
        pref.putFloat(EXP_POINTS, value);
        pref.flush();
    }

    public static float getExp() {
        pref = getPreferences();
        return pref.getFloat(EXP_POINTS, DEFAULT_EXP);
    }

    public static void saveUpgradePoints(int upgradePoints) {
        pref = getPreferences();
        pref.putInteger(UPGRADE_POINTS, upgradePoints);
        pref.flush();
    }

    public static int getUpgradePoints() {
        pref = getPreferences();
        return pref.getInteger(UPGRADE_POINTS, DEFAULT_UPGRADE_POINTS);
    }

    public static void saveMaxBookCount(int maxBookCount) {
        pref = getPreferences();
        pref.putInteger(MAX_BOOK_COUNT, maxBookCount);
        pref.flush();
    }

    public static int getMaxBookCount() {
        pref = getPreferences();
        return pref.getInteger(MAX_BOOK_COUNT, DEFAULT_MAX_BOOK_COUNT);
    }

    public static void savePerBookExperience(int perBookExp) {
        pref = getPreferences();
        pref.putInteger(PER_BOOK_EXP, perBookExp);
        pref.flush();
    }

    public static int getPerBookExperience() {
        pref = getPreferences();
        return pref.getInteger(PER_BOOK_EXP, DEFAULT_PER_BOOK_EXP);
    }

//    public static void saveChapter(int value) {
//        pref = getPreferences();
//        pref.putInteger(BOOKS_CHAPTER, value);
//        pref.flush();
//    }
//
//    public static int getChapter() {
//        pref = getPreferences();
//        return pref.getInteger(BOOKS_CHAPTER, DEFAULT_CHAPTER);
//    }


    public static void saveMoney(int value) {
        pref = getPreferences();
        pref.putInteger(MONEY_AMOUNT, value);
        pref.flush();
    }

    public static int getMoney() {
        pref = getPreferences();
        return pref.getInteger(MONEY_AMOUNT, DEFAULT_MONEY_VAL);
    }


    public static void saveLevel(int value) {
        pref = getPreferences();
        pref.putInteger(LEVEL, value);
        pref.flush();
    }

    public static int getLevel() {
        pref = getPreferences();
        return pref.getInteger(LEVEL, DEFAULT_LEVEL);
    }

    public static void saveBooks(float value) {
        pref = getPreferences();
        pref.putFloat(BOOKS_AMOUNT, value);
        pref.flush();
    }

    public static float getBooks() {
        pref = getPreferences();
        return pref.getFloat(BOOKS_AMOUNT, DEFAULT_BOOKS_AMOUNT);
    }


    public static void saveProgress(float percent) {
        pref = getPreferences();
        pref.putFloat(LEVEL_PROGRESS, percent);
        pref.flush();
    }

    public static float getProgress() {
        pref = getPreferences();
        return pref.getFloat(LEVEL_PROGRESS, DEFAULT_PROGRESS);
    }

    public static long getLastTimeout() {
        pref = getPreferences();
        return pref.getLong(LAST_TIMEOUT, System.currentTimeMillis() / 1000);
    }

    public static void saveLastTimeout(long currentTimestamp) {
        pref = getPreferences();
        pref.putLong(LAST_TIMEOUT, currentTimestamp);
        pref.flush();
    }

    public static void saveHighScores(Hashtable hashtable) {
        pref = getPreferences();
        pref.put(hashtable);
        pref.flush();
    }

    public static int[] getHighScore() {
        pref = getPreferences();
        //String data = pref.getString(HIGH_SCORE,"");
        String serializedInts = pref.getString(HIGH_SCORE, "");

        if (serializedInts.isEmpty())
            return new int[]{0, 0, 0};
        else {
            int[] deserializedInts = json.fromJson(int[].class, serializedInts);
          /*  JsonValue json = new JsonReader().parse(data);
            JsonValue highScoreJson = json.get("highscore");
             int i=0;
            int[] val= new int[3];
            for (JsonValue hs : highScoreJson.iterator())
            {
             val[i]=hs.getInt("highscore",0);
            }*/
            return json.fromJson(int[].class, serializedInts);
        }
    }

    public static boolean getSoundMute() {
        pref = getPreferences();
        return pref.getBoolean(SOUND_MUTE, DEFAULT_MUTE);
    }

    public static void saveSoundMute(boolean val) {
        pref = getPreferences();
        pref.putBoolean(SOUND_MUTE, val);
        pref.flush();
    }
    public static boolean getMusicMute() {
        pref = getPreferences();
        return pref.getBoolean(MUSIC_MUTE, DEFAULT_MUTE);
    }

    public static void saveMusicMute(boolean val) {
        pref = getPreferences();
        pref.putBoolean(MUSIC_MUTE, val);
        pref.flush();
    }

    //PIZZA MINIGAME FUNCTION
    public static void saveEraserModificator(int val) {
        pref = getPreferences();
        pref.putInteger(ERASER_MODIFICATORS_COUNTER, val);
        pref.flush();
    }

    public static int getEraserModidificator() {
        pref = getPreferences();
        return pref.getInteger(ERASER_MODIFICATORS_COUNTER, ERASER_MODIFICATORS_DEFAULT_VALUE);
    }

    public static void saveTrashCanModificator(int val) {
        pref = getPreferences();
        pref.putInteger(TRASH_CAN_MODIFIERS_COUNTER, val);
        pref.flush();
    }

    public static int getTrashCanModidificator() {
        pref = getPreferences();
        return pref.getInteger(TRASH_CAN_MODIFIERS_COUNTER, TRASH_CAN_MODIFIERS_DEFAULT_VALUE);
    }

    public static void saveTornadoModificator(int val) {
        pref = getPreferences();
        pref.putInteger(TORNADO_MODIFICATORS_COUNTER, val);
        pref.flush();
    }

    public static int getTornadoModidificator() {
        pref = getPreferences();
        return pref.getInteger(TORNADO_MODIFICATORS_COUNTER, TORNADO_MODIFICATORS_DEFAULT_VALUE);
    }

    public static void getSingleHighScore() {

    }
    // /PIZZA MINIGAME FUNCTION
    public static boolean  getFirstTime(){
        pref = getPreferences();
        return pref.getBoolean(FIRST_TIME_INSTRUCTION, FIRST_TIME_DEFAULT_VALUE);
    }
    public static void saveFirstTime(){
        pref = getPreferences();
        pref.putBoolean(FIRST_TIME_INSTRUCTION, false);
        pref.flush();
    }

    public static void saveLastInterstitialTimestamp(){
        pref= getPreferences();
        pref.putLong( LAST_INTERSTITIAL_TIMESTAMP,System.currentTimeMillis());
        pref.flush();
    }

    public static long getLastInterstitialAddTimestamp()
    {
        pref = getPreferences();
        return pref.getLong( LAST_INTERSTITIAL_TIMESTAMP ,0);
    }
}
