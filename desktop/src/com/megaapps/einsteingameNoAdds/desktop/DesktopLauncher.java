package com.megaapps.einsteingameNoAdds.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.Game;

public class DesktopLauncher
{
	public static void main( String[] arg )
	{
		InterfaceBinder binder = new InterfaceBinder();
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = ( int ) ( Constants.GAME_SIZE_WIDTH / 2f + 0.5f );
		config.height = ( int ) ( Constants.GAME_SIZE_HEIGHT / 2f + 0.5f );

		new LwjglApplication( new Game( binder, binder ), config );
	}
}
