package com.megaapps.einsteingameNoAdds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.LoadingScreen;
import com.megaapps.einsteingameNoAdds.states.State;

import java.util.Stack;

/**
 * Created by k on 14.04.2017.
 */

public class GameStateManager
{
	private int width;
	private int height;
	private LoadingScreen loadingScreen;
	private NewAssetManager assetManager;
	private Stack<State> states;
	private boolean assetAsynLoadStarted = false;
	private boolean assetAsynLoadFinished = false;

	private Array<DelayedAsyncAction> delayedActions;

	public GameStateManager( NewAssetManager am, LoadingScreen ls )
	{
		delayedActions = new Array<DelayedAsyncAction>();
		states = new Stack<State>();
		assetManager = am;
		loadingScreen = ls;
	}

	public void push( State state )
	{
		invalidateInput();

		states.push( state );

		delayedActions.clear();
		delayedActions.add( new DelayedAsyncAction( DelayedAsyncActionType.SetInputs ) );
		delayedActions.add( new DelayedAsyncActionResize( width, height ) );

		state.loadAssets();
		assetAsynLoadStarted = false;
		assetAsynLoadFinished = false;

		Gdx.app.log( "StateManager", "State push " + state.getClass().getSimpleName() );
	}

	public void add( State state )
	{
		invalidateInput();

		states.push( state );

		delayedActions.clear();
		delayedActions.add( new DelayedAsyncAction( DelayedAsyncActionType.SetInputs ) );
		delayedActions.add( new DelayedAsyncActionResize( width, height ) );

		state.loadAssets();
		assetAsynLoadStarted = false;
		assetAsynLoadFinished = false;

		Gdx.app.log( "StateManager", "State add " + state.getClass().getSimpleName() );
	}

	public void popDispose()
	{
		states.pop().dispose();
		states.peek().setInputs();

		Gdx.app.log( "StateManager", "State popDispose " + states.peek().getClass().getSimpleName() );
	}

	public void set( State state )
	{
		invalidateInput();

		Gdx.app.log( "StateManager", "State popDispose " + states.peek().getClass().getSimpleName() );
		Gdx.app.log( "StateManager", "State set " + state.getClass().getSimpleName() );

		states.pop().dispose();
		states.push( state );

		delayedActions.clear();
		delayedActions.add( new DelayedAsyncAction( DelayedAsyncActionType.SetInputs ) );
		delayedActions.add( new DelayedAsyncActionResize( width, height ) );

		state.loadAssets();
		assetAsynLoadStarted = false;
		assetAsynLoadFinished = false;
	}

	public void render( float dt, SpriteBatch sb )
	{
		State state = states.peek();

		// Wait till all assets are loaded
		if( !assetManager.isAsyncLoadFinished() )
		{
			// Clear color on edges
			Gdx.gl.glClearColor( 26 / 255f, 24 / 255f, 35 / 255f, 1 );

			// Show loading screen and init its transitions
			if( !assetAsynLoadStarted )
			{
				// TODO: initialize loading screen fade in fade out
				loadingScreen.resize( width, height );
				loadingScreen.show();
				assetAsynLoadStarted = true;
			}

			loadingScreen.setLoadProgress( assetManager.getLoadProgress() );
			loadingScreen.act();
			loadingScreen.draw();

			return;
		}

		// Assets loading finished, initialize state
		if( !assetAsynLoadFinished )
		{
			loadingScreen.hide();
			state.initialize();

			if( state.isInitialized() )
				applyDelayedActions( state );
			else
				Gdx.app.error( "GamesStateManager", "State " + state.getClass().getSimpleName() + " falied to initialize!!" );

			assetAsynLoadFinished = true;
		}

		// Update state
		state.update( dt );
		state.render( sb );
	}

	private void applyDelayedActions( State state )
	{
		for( DelayedAsyncAction a : delayedActions )
		{
			a.apply( state );
		}

		delayedActions.clear();
	}

	public void resize( int width, int height )
	{
		State state = states.peek();
		this.width = width;
		this.height = height;

		if( state.isInitialized() )
			state.resize( width, height );
		else
			delayedActions.add( new DelayedAsyncActionResize( width, height ) );

		// Also resize loading screen
		if( loadingScreen.isShown() )
			loadingScreen.resize( width, height );
	}

	public void resume()
	{
		State state = states.peek();
		if( state.isInitialized() )
			state.resume();
		else
			delayedActions.add( new DelayedAsyncAction( DelayedAsyncActionType.Resume ) );

	}

	public void pause()
	{
		State state = states.peek();
		if( state.isInitialized() )
			state.pause( true );
		else
			delayedActions.add( new DelayedAsyncAction( DelayedAsyncActionType.Pause ) );
	}

	private void invalidateInput()
	{
		Gdx.input.setInputProcessor( null );
	}

	public void invalidate()
	{
		State state = states.peek();
		if( state.isInitialized() )
		{
			state.resize( width, height );
			state.setInputs();
		}
		else
		{
			delayedActions.add( new DelayedAsyncAction( DelayedAsyncActionType.SetInputs ) );
			delayedActions.add( new DelayedAsyncActionResize( width, height ) );
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	enum DelayedAsyncActionType
	{
		Resize, Pause, Resume, SetInputs, NOT_SET
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	class DelayedAsyncAction
	{
		protected DelayedAsyncActionType type = DelayedAsyncActionType.NOT_SET;

		public DelayedAsyncAction( DelayedAsyncActionType type )
		{
			this.type = type;
		}

		public void apply( State state )
		{
			//Gdx.app.log( "DeleyedAsyncAction", "Applying action " + type.toString() + " to state " + state.getClass().getSimpleName() );

			switch( type )
			{
				case Resize:
					resizeDeleyedAction( state );
					break;
				case Pause:
					state.pause( true );
					break;
				case Resume:
					state.resume();
					break;
				case SetInputs:
					state.setInputs();
					break;
				case NOT_SET:
					Gdx.app.error( "DelayedAsyncAction", "Action type not set!" );
					break;
			}
		}

		protected void resizeDeleyedAction( State state )
		{
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	class DelayedAsyncActionResize extends DelayedAsyncAction
	{
		private int width;
		private int height;

		public DelayedAsyncActionResize( int width, int height )
		{
			super( DelayedAsyncActionType.Resize );
			this.width = width;
			this.height = height;
		}

		@Override
		protected void resizeDeleyedAction( State state )
		{
			state.resize( width, height );
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

}
