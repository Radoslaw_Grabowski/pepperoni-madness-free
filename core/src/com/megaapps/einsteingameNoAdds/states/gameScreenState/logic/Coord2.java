package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;

/**
 * Created by lukasz on 05.05.17.
 */

public class Coord2 {
    private int x, y;

    public Coord2(int x, int  y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public int metricDist(Coord2 coord) {
        return Math.abs(coord.getX() - this.x) +
                Math.abs(coord.getY() - this.y);
    }

    public double dist(Coord2 coord) {
        return Math.sqrt((this.x - coord.getX())*(this.x - coord.getX()) +
                (this.y - coord.getY())*(this.y - coord.getY()));
    }
}
