package com.megaapps.einsteingameNoAdds.states.singleCutSceneState;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megaapps.einsteingameNoAdds.GameStateManager;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.State;

/**
 * Created by k on 10.05.2017.
 */

public class SingleSceneState extends State
{
	ComicStage comicStage;
	private int id;

	public SingleSceneState( GameStateManager gsm, NewAssetManager as, int id )
	{
		super( gsm, as );
		this.id = id;
	}

	@Override
	public void initialize()
	{
		comicStage = new ComicStage( id );

		isInitialized = true;
	}

	@Override
	public void loadAssets()
	{

	}

	@Override
	public void pause( boolean pauseMusic )
	{

	}

	@Override
	public void update( float dt )
	{
		comicStage.act( dt );
	}

	@Override
	public void render( SpriteBatch sb )
	{
		comicStage.draw();
	}

	@Override
	public void dispose()
	{
		comicStage.dispose();
	}

	@Override
	public void resize( int width, int height )
	{
		comicStage.getViewport().update( width, height );
	}

	@Override
	public void resume()
	{

	}

	@Override
	public void setInputs()
	{

	}
}
