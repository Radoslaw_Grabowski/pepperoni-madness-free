package com.megaapps.einsteingameNoAdds.states.tutorialScreenState;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.GameStateManager;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.State;
import com.megaapps.einsteingameNoAdds.states.gameOnlyStates.GameOnlyMainScreenState;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.GameScreenState;

/**
 * Created by Kamil on 6/23/2017.
 */

public class TutorialState extends State
{
	private Stage tutorialStage;

	public TutorialState( GameStateManager gameStateManager, NewAssetManager as )
	{
		super( gameStateManager, as );
	}

	@Override
	public void initialize()
	{
		tutorialStage = new Stage( new FitViewport( Constants.GAME_SIZE_WIDTH, Constants.GAME_SIZE_HEIGHT ) );
		final Image slide1, slide2, slide3;

		slide1 = new Image( assetManager.get( Constants.PIZZA_GAME_TUTORIAL_TUT1_PATH, Texture.class ) );
		slide2 = new Image( assetManager.get( Constants.PIZZA_GAME_TUTORIAL_TUT2_PATH, Texture.class ) );
		slide3 = new Image( assetManager.get( Constants.PIZZA_GAME_TUTORIAL_TUT3_PATH, Texture.class ) );

		tutorialStage.addActor( slide1 );
		slide1.addListener( new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				tutorialStage.addActor( slide2 );
			}
		} );

		slide2.addListener( new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				tutorialStage.addActor( slide3 );
			}
		} );

		slide3.addListener( new ClickListener()
		{
			@Override
			public void clicked( InputEvent event, float x, float y )
			{
				PreferencesHelper.saveFirstTime();
				GameScreenState m_gameScreenState = new GameScreenState( gsm, assetManager );
				gsm.set( new GameOnlyMainScreenState( gsm, assetManager, m_gameScreenState ) );
			}
		} );

		Gdx.gl.glClearColor( Constants.TUTORIAL_CLEAR_COLORS.x, Constants.TUTORIAL_CLEAR_COLORS.y, Constants.TUTORIAL_CLEAR_COLORS.z, 1 );

		isInitialized = true;
	}

	@Override
	public void loadAssets()
	{
		assetManager.loadAsync( Constants.FONT_PATH,                                                             BitmapFont.class );
		assetManager.loadAsync( Constants.GAME_LOGO_PATH,                                                        Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_ERASER_PATH,                                         Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_MODIFIER_BACKGROUND_PATH,                            Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BACKGROUND_PATH,                    Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BLACK_LABEL_PATH,                   Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BOTTOM_LABEL_PATH,                  Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_TRASH_CAN_PATH,                     Texture.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_TORNADO_PATH,                                        Texture.class );
		assetManager.loadAsync( Constants.MINI_GAME_TUTORIAL_STATE_GAME_TUTORIAL_PATH,                           Texture.class );
		assetManager.loadAsync( Constants.PIZZA_GAME_TUTORIAL_TUT1_PATH,                                         Texture.class );
		assetManager.loadAsync( Constants.PIZZA_GAME_TUTORIAL_TUT2_PATH,                                         Texture.class );
		assetManager.loadAsync( Constants.PIZZA_GAME_TUTORIAL_TUT3_PATH,                                         Texture.class );

		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_ELEMENTS_ATLAS_PATH,                TextureAtlas.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_ELEMENTS_PIZZA_ELEMENTS_ATLAS_PATH, TextureAtlas.class );
		assetManager.loadAsync( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_PIZZA_MENU_ATLAS_PATH,              TextureAtlas.class );

		assetManager.loadAsync( Constants.THEME_MUSIC_PATH,                                                      Music.class );

		assetManager.loadAsync( Constants.CLICK_SOUND_PATH,                                                      Sound.class );
		assetManager.loadAsync( Constants.REMOVE_PIZZA_PATH,                                                     Sound.class );
	}

	@Override
	public void pause( boolean pauseMusic )
	{

	}

	@Override
	public void update( float dt )
	{
		tutorialStage.act( dt );
	}

	@Override
	public void render( SpriteBatch sb )
	{
		tutorialStage.draw();
	}

	@Override
	public void dispose()
	{
		tutorialStage.dispose();
	}

	@Override
	public void resize( int width, int height )
	{
		tutorialStage.getViewport().update( width, height );
	}

	@Override
	public void resume()
	{

	}

	@Override
	public void setInputs()
	{
		Gdx.input.setInputProcessor( tutorialStage );
	}
}
