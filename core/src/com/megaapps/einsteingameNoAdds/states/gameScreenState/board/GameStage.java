package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.Game;
import com.megaapps.einsteingameNoAdds.GameStateManager;
import com.megaapps.einsteingameNoAdds.events.AddModificatorsPoints;
import com.megaapps.einsteingameNoAdds.events.ReturnToMainScreenEvent;
import com.megaapps.einsteingameNoAdds.events.ShowComboEvent;
import com.megaapps.einsteingameNoAdds.events.dialogs.CancelDialogEvent;
import com.megaapps.einsteingameNoAdds.exceptions.GameOverException;
import com.megaapps.einsteingameNoAdds.exceptions.InterceptFailedException;
import com.megaapps.einsteingameNoAdds.helpers.ElementColorHelper;
import com.megaapps.einsteingameNoAdds.helpers.FontHelper;
import com.megaapps.einsteingameNoAdds.helpers.MusicHelper;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.interfaces.AddsInterface;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord2;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord3;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Element;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.GameLogic;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.GameLogicFactory;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;

import static com.megaapps.einsteingameNoAdds.Constants.ADD_HEIGHT;
import static com.megaapps.einsteingameNoAdds.Constants.ELEMENT_SIZE;
import static com.megaapps.einsteingameNoAdds.Constants.GAME_SCREEN_STATE_ERASER_PATH;
import static com.megaapps.einsteingameNoAdds.Constants.GAME_SCREEN_STATE_TORNADO_PATH;
import static com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_HEIGHT;
import static com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_WIDTH;
import static com.megaapps.einsteingameNoAdds.Constants.MINIGAME_BOTTOM_LABEL_BLACK;
import static com.megaapps.einsteingameNoAdds.Constants.MINIGAME_BOTTOM_LABEL_BLACK_HEIGHT;
import static com.megaapps.einsteingameNoAdds.Constants.MODIFIER_SIZE;
import static com.megaapps.einsteingameNoAdds.Game.bus;

/**
 * Created by lukasz on 28.04.17.
 */

public class GameStage extends Stage implements GestureDetector.GestureListener
{

	private static final String TAG = GameStage.class.getSimpleName();

	private static final float HUD_HORIZONTAL_PADDING = GAME_SIZE_WIDTH / 20;
	private static final float DRAGGABLE_IMAGE_PADDING = ( GAME_SIZE_WIDTH - ( 3 * ELEMENT_SIZE ) ) / 4;
	private static final int MODIFICATORS_REVARD = 5;

	private OrthographicCamera camera;
	private Label scoreLabel, discardsLabel, highScoreLabel, comboLabel;
	private Group trashCounterContainer;
	private TextureAtlas elementAtlas, pizzaMenuAtlas;
	private Container scoreLabelContainer, comboContainer;
	private int score = 0;
	private Button pauseButton, buttonTrashCan;
	private com.megaapps.einsteingameNoAdds.states.gameScreenState.board.GameBoard gameBoard;
	private GameLogic gameLogic;
	private com.megaapps.einsteingameNoAdds.states.gameScreenState.board.DraggableImage draggableImage1, draggableImage2, draggableImage3;
	private DraggableEraser draggableEraser;
	private com.megaapps.einsteingameNoAdds.states.gameScreenState.board.DraggableTornado draggableTornado;
	private GestureDetector gestureDetector;
	private Image trashCounterContainerBackground;
	private Rectangle gameBoardBounds;
	private Image eraserLabelBackground, tornadoBackground, tornadoLabelBackground, blackLabel;
	private Label erasesLabel;
	private Label tornadoLabel;
	private ShapeRenderer shapeRenderer;
	private DelayedRemovalArray<com.megaapps.einsteingameNoAdds.states.gameScreenState.board.GraphicalElement> graphicalElements = new DelayedRemovalArray<com.megaapps.einsteingameNoAdds.states.gameScreenState.board.GraphicalElement>( 50 );
	private NewAssetManager assetManager;
	public enum DraggableImageActionTypeName
	{
		TOUCH_DOWN, PAN, PAN_STOP
	}
	public GameStage( GameStateManager gsm, NewAssetManager assetManager )
	
	{
		this.assetManager = assetManager;
		bus.register(this);
		setViewport(new FitViewport( GAME_SIZE_WIDTH, GAME_SIZE_HEIGHT));
		gameLogic = new GameLogicFactory().create(Constants.GameDifficulty.EASY);
		camera = new OrthographicCamera(getViewport().getScreenWidth(), getViewport().getScreenHeight());
		getViewport().setCamera(camera);
		getCamera().position.set(GAME_SIZE_WIDTH / 2, GAME_SIZE_HEIGHT / 2, 0);
       /* Image backgroundImage = new Image(assetManager.getTextureAssets("gameScreenState/singleMiniGame/background.png"));
        backgroundImage.setSize(GAME_SIZE_WIDTH, GAME_SIZE_HEIGHT);
        addActor(backgroundImage);*/
		elementAtlas = assetManager.get("gameScreenState/singleMiniGame/elements/pizzaElements.atlas",TextureAtlas.class);
		pizzaMenuAtlas = assetManager.get("gameScreenState/singleMiniGame/pizzaMenu.atlas",TextureAtlas.class);
		//   Image boardImage = new Image(assetManager.getTextureAssets("gameScreenState/boardBackground.png"));

		shapeRenderer = new ShapeRenderer();

		// boardImage.setSize(GAME_SIZE_WIDTH* .95f, GAME_SIZE_HEIGHT * .77f);
		// boardImage.setPosition(GAME_SIZE_WIDTH / 2 - boardImage.getWidth() / 2, GAME_SIZE_HEIGHT * .1f);
		//  addActor(boardImage);


   /*     Image elementDrawer  = new Image(assetManager.getTextureAssets("gameScreenState/singleMiniGame/blackLabel.png"));
        elementDrawer.setSize(GAME_SIZE_WIDTH, ELEMENT_DRAWER_HEIGHT);
        elementDrawer.setPosition(GAME_SIZE_WIDTH / 2, BOTTOM_TAB_HEIGHT, Align.center);
        addActor(elementDrawer);
*/



		Table bottomTableModifications = new Table();
		Table trashCanTable = new Table();


		Group buttonTrashCan = new Group();
		buttonTrashCan.setSize(MODIFIER_SIZE, MODIFIER_SIZE);
		Image modifiersBackground = new Image(assetManager.get("gameScreenState/modifierBackground.png",Texture.class));
		modifiersBackground.setSize(MODIFIER_SIZE, MODIFIER_SIZE);
		Color tintingColor = ElementColorHelper.getGreenGameColor();
		modifiersBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
		Image trashCanImage = new Image(assetManager.get("gameScreenState/singleMiniGame/trashCan.png",Texture.class));
		trashCanImage.setSize(MODIFIER_SIZE, MODIFIER_SIZE);
		buttonTrashCan.addActor(modifiersBackground);
		buttonTrashCan.addActor(trashCanImage);

		buttonTrashCan.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				MusicHelper.playClickSound();
				if (gameLogic.getElementDiscardsLeft() == 0)
					Game.addsInterface.showRequestVideoAdd(true, AddsInterface.ModificatorType.TRASH_CAN);
				else
					removeElement();
			}
		});

      /*  buttonTrashCan = new Button(style);
        buttonTrashCan.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                MusicHelper.playClickSound();
                removeElement();
            }
        });
        buttonTrashCan.setSize(MODIFIER_SIZE, MODIFIER_SIZE);*/

/*

        buttonTrashCan.addListener(new ClickListener() {
                                       @Override
                                       public void clicked(InputEvent event, float x, float y) {
                                           MusicHelper.playClickSound();
                                           removeElement();
                                       }
                                   }
        );
*/

		//  buttonTrashCanTable.pack();
		trashCounterContainer = new Group();
		trashCounterContainerBackground = new Image(pizzaMenuAtlas.findRegion("counterWhite"));
		trashCounterContainerBackground.setSize(buttonTrashCan.getWidth() * 1.2f, trashCounterContainerBackground.getHeight());


		Label.LabelStyle counterStyle = new Label.LabelStyle( assetManager.get( Constants.FONT_XX_SMALL_PATH,BitmapFont.class), Color.BLACK);

		discardsLabel = new Label("", counterStyle);
		discardsLabel.setAlignment(Align.center);
		trashCounterContainer.setSize(trashCounterContainerBackground.getWidth(), trashCounterContainerBackground.getHeight());
		trashCounterContainer.addActor(trashCounterContainerBackground);
		trashCounterContainer.addActor(discardsLabel);
		trashCounterContainer.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (gameLogic.getElementDiscardsLeft() == 0)
					Game.addsInterface.showRequestVideoAdd(true, AddsInterface.ModificatorType.TRASH_CAN);
			}
		});
		setDiscardsLabel(gameLogic.getElementDiscardsLeft());
		trashCanTable.add(buttonTrashCan).size(buttonTrashCan.getWidth(), buttonTrashCan.getHeight()).align(Align.center);
		trashCanTable.row();
		trashCanTable.add(trashCounterContainer).size(trashCounterContainer.getWidth(), trashCounterContainer.getHeight()).align(Align.center);
		trashCanTable.pack();


		// eraser
		Table eraserTable = new Table();

		Group eraserDraggableContainer = new Group();
		Image eraserBackground = new Image(new Texture("gameScreenState/modifierBackground.png"));
		eraserBackground.setSize(MODIFIER_SIZE, MODIFIER_SIZE);
		eraserBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
		eraserDraggableContainer.addActor(eraserBackground);
		eraserDraggableContainer.setSize(eraserBackground.getWidth(), eraserBackground.getHeight());

		Group eraserLabelContainer = new Group();
		eraserLabelBackground = new Image(pizzaMenuAtlas.findRegion("counterWhite"));
		eraserLabelBackground.setSize(eraserDraggableContainer.getWidth() * 1.2f, eraserLabelBackground.getHeight());
		eraserLabelBackground.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (gameLogic.getErasesLeft() == 0)
					Game.addsInterface.showRequestVideoAdd(true, AddsInterface.ModificatorType.ERASER);
			}
		});
		eraserLabelContainer.addActor(eraserLabelBackground);


		Label.LabelStyle eraserLabelStyle = new Label.LabelStyle( assetManager.get( Constants.FONT_XX_SMALL_PATH,BitmapFont.class),  Color.BLACK);
		erasesLabel = new Label(String.valueOf(gameLogic.getErasesLeft()), eraserLabelStyle);
		setEraseLabel(gameLogic.getErasesLeft());
		erasesLabel.setAlignment(Align.center);

		eraserLabelContainer.addActor(erasesLabel);
		eraserLabelContainer.setSize(eraserLabelBackground.getWidth(), eraserLabelBackground.getHeight());
		eraserTable.add(eraserDraggableContainer).size(eraserDraggableContainer.getWidth(), eraserDraggableContainer.getHeight()).align(Align.center).row();
		eraserTable.add(eraserLabelContainer).size(eraserLabelContainer.getWidth(), eraserLabelContainer.getHeight()).align(Align.center);
		eraserTable.pack();


		Table tornadoTable = new Table();

		Group tornadoDraggableContainer = new Group();
		tornadoBackground = new Image(new Texture("gameScreenState/modifierBackground.png"));
		tornadoBackground.setSize(MODIFIER_SIZE, MODIFIER_SIZE);
		tornadoBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
		tornadoDraggableContainer.addActor(tornadoBackground);
		tornadoDraggableContainer.setSize(tornadoBackground.getWidth(), tornadoBackground.getHeight());

		Group tornadoLabelContainer = new Group();

		tornadoLabelBackground= new Image(pizzaMenuAtlas.findRegion("counterWhite"));

		tornadoLabelBackground.setSize(tornadoDraggableContainer.getWidth() * 1.2f, tornadoLabelBackground.getHeight());
		tornadoLabelContainer.addActor(tornadoLabelBackground);

		Label.LabelStyle tornadoLabelStyle = new Label.LabelStyle( assetManager.get( Constants.FONT_XX_SMALL_PATH,BitmapFont.class),  Color.BLACK);

		tornadoLabel = new Label(String.valueOf(""), tornadoLabelStyle);
		tornadoLabel.setAlignment(Align.center);
		tornadoLabel.setWidth(tornadoLabelBackground.getWidth());
		tornadoLabelContainer.addActor(tornadoLabel);
		tornadoLabelContainer.setSize(tornadoLabelBackground.getWidth(), tornadoLabelBackground.getHeight());
		tornadoLabelContainer.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (gameLogic.getTornadoLeft() == 0)
					Game.addsInterface.showRequestVideoAdd(true, AddsInterface.ModificatorType.TORNADO);
			}
		});
		tornadoTable.add(tornadoDraggableContainer).size(tornadoDraggableContainer.getWidth(), tornadoDraggableContainer.getHeight()).align(Align.center).row();
		tornadoTable.add(tornadoLabelContainer).size(tornadoLabelContainer.getWidth(), tornadoLabelContainer.getHeight()).align(Align.center);
		tornadoTable.pack();
		setTornadoLabel(gameLogic.getTornadoLeft());
		setEraseLabel(gameLogic.getErasesLeft());
		float padding = (GAME_SIZE_WIDTH / 10f);
		bottomTableModifications.add(trashCanTable).padRight(padding);
		bottomTableModifications.add(eraserTable).padRight(padding);
		bottomTableModifications.add(tornadoTable);
		bottomTableModifications.pack();
		bottomTableModifications.setPosition(GAME_SIZE_WIDTH / 2 - bottomTableModifications.getWidth() / 2, GAME_SIZE_HEIGHT / 55);
		addActor(bottomTableModifications);

		draggableEraser = new DraggableEraser(assetManager.get(GAME_SCREEN_STATE_ERASER_PATH ,Texture.class), bottomTableModifications.getX() +
																											   (bottomTableModifications.getWidth()/2) - MODIFIER_SIZE/2, bottomTableModifications.getY() + eraserLabelContainer.getHeight());

		draggableTornado = new DraggableTornado(assetManager.get(GAME_SCREEN_STATE_TORNADO_PATH  ,Texture.class), bottomTableModifications.getX() +
																												  bottomTableModifications.getWidth() - (MODIFIER_SIZE + eraserDraggableContainer.getWidth() / 10), bottomTableModifications.getY() + eraserLabelContainer.getHeight());

		// elements draggable image 1
		draggableImage1 = new DraggableImage(gameLogic.getCurrentElements(),assetManager);
		draggableImage1.setPositionAndSetupBounds((int) GAME_SIZE_WIDTH / 2, (int) (MINIGAME_BOTTOM_LABEL_BLACK + MINIGAME_BOTTOM_LABEL_BLACK_HEIGHT / 2));


       /* // elements draggable image 2
        draggableImage2 = new DraggableImage(gameLogic.getCurrentElements());
        draggableImage2.setPositionAndSetupBounds((int)(draggableImage1.getX()+draggableImage1.getWidth()+DRAGGABLE_IMAGE_PADDING),(int)BOTTOM_TAB_HEIGHT);


        // elements draggable image 3
        draggableImage3 = new DraggableImage(gameLogic.getCurrentElements());
        draggableImage3.setPositionAndSetupBounds((int)(draggableImage2.getX()+draggableImage2.getWidth()+DRAGGABLE_IMAGE_PADDING),(int)BOTTOM_TAB_HEIGHT);*/


		gameBoard = new GameBoard(gameLogic,assetManager);
		gameBoardBounds = new Rectangle(gameBoard.getX(), gameBoard.getY(), gameBoard.getWidth(), gameBoard.getHeight());
		addActor(gameBoard);
		addActor(draggableImage1);
		addActor(draggableEraser);
		addActor(draggableTornado);
		//  addActor(draggableImage2);
		// addActor(draggableImage3);




		Button.ButtonStyle pauseButtonStyle = new Button.ButtonStyle();
		pauseButtonStyle.up = new Image(pizzaMenuAtlas.findRegion("pauza")).getDrawable();
		pauseButtonStyle.down = new Image(pizzaMenuAtlas.findRegion("pauzaPressed")).getDrawable();
		pauseButton = new Button(pauseButtonStyle);
		float ratio = pauseButton.getWidth() / pauseButton.getHeight();
		pauseButton.setSize(ratio * GAME_SIZE_HEIGHT * .06f, GAME_SIZE_HEIGHT * .06f);
		pauseButton.setPosition(GAME_SIZE_WIDTH - (pauseButton.getWidth() + HUD_HORIZONTAL_PADDING),
								GAME_SIZE_HEIGHT - (pauseButton.getHeight() * 1.7f + HUD_HORIZONTAL_PADDING / 2) - 3.5f * ADD_HEIGHT);

		pauseButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				MusicHelper.playClickSound();
				super.clicked(event, x, y);
				if(System.currentTimeMillis()-PreferencesHelper.getLastInterstitialAddTimestamp()> 10 * 60 * 1000)
					Game.addsInterface.showInterstitialAdd();
				bus.post(new ReturnToMainScreenEvent(score, false));
			}
		});
		addActor(pauseButton);

		Label.LabelStyle labelStyle = new Label.LabelStyle( assetManager.get( Constants.FONT_PATH,BitmapFont.class), Color.WHITE);
		scoreLabel = new Label(String.valueOf(score), labelStyle);
		scoreLabelContainer = new Container(scoreLabel);
		scoreLabelContainer.setTransform(true);
		scoreLabelContainer.setSize(GAME_SIZE_WIDTH, scoreLabel.getHeight());
		// scoreLabelContainer.setPosition(0, (boardImage.getY() + boardImage.getHeight()) - (GAME_SIZE_HEIGHT * .06f + scoreLabel.getHeight()));
		scoreLabelContainer.setPosition(0,pauseButton.getY());
		addActor(scoreLabelContainer);
		setInputs();

		Table highScoreContainer = new Table();

		Label.LabelStyle highScoreStyle = new Label.LabelStyle( assetManager.get( Constants.FONT_PATH,BitmapFont.class), Color.YELLOW);
		Label.LabelStyle titleHighScoreStyle = new Label.LabelStyle( assetManager.get( Constants.FONT_PATH,BitmapFont.class), Color.WHITE);
		Label titleLabel = new Label("Best Score", titleHighScoreStyle);
		highScoreLabel = new Label(String.valueOf(gameLogic.getHighScore()), highScoreStyle);
		highScoreLabel.setAlignment(Align.center);
		highScoreContainer.add(titleLabel).size(titleLabel.getWidth(), titleLabel.getHeight()).align(Align.center).row();
		highScoreContainer.add(highScoreLabel).size(titleLabel.getWidth(), highScoreLabel.getHeight()).align(Align.center);
		highScoreContainer.pack();
		highScoreContainer.setPosition(HUD_HORIZONTAL_PADDING,
									   scoreLabelContainer.getY());
		addActor(highScoreContainer);
		initComboLabel();
	}

	public void invalidateHighScore(){
		highScoreLabel.setText(String.valueOf(PreferencesHelper.getHighScore()[0]));
		highScoreLabel.setAlignment(Align.center);
	}

	public void resetGame() {
		if( gameLogic != null )
			gameLogic.resetGame();

		for (GraphicalElement g : graphicalElements) {
			g.remove();
		}
		graphicalElements.clear();
		score = 0;
		scoreLabel.setText(String.valueOf(gameLogic.getScore()));
		scoreLabel.setAlignment(Align.center);
		highScoreLabel.setText(String.valueOf(PreferencesHelper.getHighScore()[0]));
		highScoreLabel.setAlignment(Align.center);
	}

	private void initComboLabel()
	{
		Label.LabelStyle labelStyle = new Label.LabelStyle(  assetManager.get( Constants.FONT_PATH,BitmapFont.class), Color.WHITE );
		comboLabel = new Label( "", labelStyle );

		comboContainer = new Container( comboLabel );
		comboContainer.setPosition( GAME_SIZE_WIDTH / 2 - comboLabel.getWidth() / 2, GAME_SIZE_HEIGHT / 2 - comboLabel.getHeight() / 2 );
		comboContainer.setVisible( false );
		comboContainer.setTransform( true );
		addActor( comboContainer );

	}

	public void setInputs()
	{
		Gdx.input.setCatchBackKey( true );
		InputProcessor backProcessor = new InputAdapter()
		{
			@Override
			public boolean keyDown( int keycode )
			{
				if( ( keycode == Input.Keys.BACK ) )
				{
					//bus.post( new ShowConfirmationDialogEvent( "", "If you exit you will lose your score. Leave anyway?", "Leave" ) );
					bus.post(new ReturnToMainScreenEvent(score, false));
				}
				return true;
			}
		};


		gestureDetector = new GestureDetector( this );
		InputMultiplexer im = new InputMultiplexer( gestureDetector, backProcessor, this );
		Gdx.input.setInputProcessor( im );
	}

	public void resize( int width, int height )
	{
		this.getViewport().update( width, height );
	}

	@Override
	public void act( float delta )
	{
		super.act( delta );
		graphicalElements.begin();
		for( int i = 0; i < graphicalElements.size; i++ )
		{
			GraphicalElement graphicalElement = graphicalElements.get( i );
			if( graphicalElement.getY() + graphicalElement.getHeight() < 0 )
			{
				graphicalElements.removeIndex( i );
				graphicalElement.remove();
			}
		}
		graphicalElements.end();
	}

	@Override
	public void draw()
	{
		super.draw();
		// Draggable Image part  Debug Line
     /*  shapeRenderer.setProjectionMatrix(getBatch().getProjectionMatrix());
        shapeRenderer.setTransformMatrix(getBatch().getTransformMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.rect(draggableTornado.getBounds().getX(),draggableTornado.getBounds().getY(),
                draggableTornado.getBounds().getWidth(),draggableTornado.getBounds().getHeight());
        shapeRenderer.rect(gameBoardBounds.getX(),gameBoardBounds.getY(),
                gameBoardBounds.getWidth(),gameBoardBounds.getHeight());
        shapeRenderer.rect(draggableEraser.getBounds().getX(),draggableEraser.getBounds().getY(),
                draggableEraser.getBounds().getWidth(),draggableEraser.getBounds().getHeight());
        shapeRenderer.rect(draggableImage1.getX(),draggableImage1.getY(),draggableImage1.getWidth(),draggableImage1.getHeight());
        shapeRenderer.rect(draggableImage1.getBounds().getX(),draggableImage1.getBounds().getY(),draggableImage1.getBounds().getWidth(),draggableImage1.getBounds().getHeight());
        shapeRenderer.end();*/
		//
	}

	@Override
	public void dispose()
	{
		super.dispose();
		bus.unregister( this );
	}

	@Subscribe
	public void cancelDialog( CancelDialogEvent event )
	{
		setInputs();
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		if (!draggableImage1.isDragged() && !draggableEraser.isDragged() && !draggableTornado.isDragged()) {
			dragImagesHandler( DraggableImageActionTypeName.TOUCH_DOWN, draggableImage1, x, y);
			//   dragImagesHandler(DraggableImageActionTypeName.TOUCH_DOWN,draggableImage2,x,y);
			//   dragImagesHandler(DraggableImageActionTypeName.TOUCH_DOWN,draggableImage3,x,y);
			Vector2 position = getViewport().unproject(new Vector2(x, y)); //with viewport

			if (draggableEraser.getBounds().contains(position.x, position.y)) {

				draggableEraser.setDragged(true);
				draggableEraser.toFront();
			}
//      if (draggableTornado.getBounds().contains(position.x, position.y)) {
//            draggableTornado.setDragged(true);
//          draggableTornado.toFront();
//             }
		}

		return false;
	}

	@Override
	public boolean tap( float x, float y, int count, int button )
	{
		Vector2 position = getViewport().unproject(new Vector2(x, y)); //with viewport
		if (draggableEraser.getBounds().contains(position.x, position.y)) {
			if (gameLogic.getErasesLeft() == 0)
				Game.addsInterface.showRequestVideoAdd(true, AddsInterface.ModificatorType.ERASER);
		}

		if (draggableTornado.getBounds().contains(position.x, position.y))
		{
			MusicHelper.getClickSound();

			if (gameLogic.getTornadoLeft() == 0)
				Game.addsInterface.showRequestVideoAdd(true, AddsInterface.ModificatorType.TORNADO);
			else
			{
				int tornadosLeft = gameLogic.getTornadoLeft() - 1;
				gameLogic.setTornadoLeft(tornadosLeft);
				setTornadoLabel(tornadosLeft);
				PreferencesHelper.saveTornadoModificator(tornadosLeft);
				draggableTornado.returnToStartPosition();
				gameLogic.shuffleLogicBoards();
				shuffleGraphicalElement();
			}
		}
		return false;
	}

	@Override
	public boolean longPress( float x, float y )
	{
		return false;
	}

	@Override
	public boolean fling( float velocityX, float velocityY, int button )
	{
		return false;
	}

	@Override
	public boolean pan( float x, float y, float deltaX, float deltaY )
	{
		Vector2 position = getViewport().unproject( new Vector2( x, y ) );

		dragImagesHandler( DraggableImageActionTypeName.PAN, draggableImage1, x, y );
		//  dragImagesHandler(DraggableImageActionTypeName.PAN,draggableImage2,x,y);
		//  dragImagesHandler(DraggableImageActionTypeName.PAN,draggableImage3,x,y);

		if( draggableEraser.getBounds().contains( position.x, position.y ) && draggableEraser.isDragged() )
			draggableEraser.dragImage( position.x, position.y );
		if( draggableTornado.getBounds().contains( position.x, position.y ) && draggableTornado.isDragged() )
			draggableTornado.dragImage( position.x, position.y );


		return false;
	}

	@Override
	public boolean panStop( float x, float y, int pointer, int button )
	{

		dragImagesHandler( DraggableImageActionTypeName.PAN_STOP, draggableImage1, x, y );
		//    dragImagesHandler(DraggableImageActionTypeName.PAN_STOP,draggableImage2,x,y);
		//    dragImagesHandler(DraggableImageActionTypeName.PAN_STOP,draggableImage3,x,y);


		if( draggableTornado.isDragged() )
		{
			draggableTornado.setDragged( false );
			if( draggableTornado.getBounds().overlaps( gameBoardBounds ) )
			{

				if (gameLogic.getTornadoLeft() > 0) {
					int tornadosLeft = gameLogic.getTornadoLeft() - 1;
					gameLogic.setTornadoLeft(tornadosLeft);
					setTornadoLabel(tornadosLeft);
					PreferencesHelper.saveTornadoModificator(tornadosLeft);
					draggableTornado.returnToStartPosition();
					gameLogic.shuffleLogicBoards();
					shuffleGraphicalElement();
				} else
					draggableTornado.returnToStartPosition();

			} else
				draggableTornado.returnToStartPosition();
		}


		if (draggableEraser.isDragged()) {
			draggableEraser.setDragged(false);
			try {
				Coord2 coord = gameBoard.ifIntercept( draggableEraser);
				List<Coord3> erasedCoords = gameLogic.erase(coord);
				for (int i = 0, size = erasedCoords.size(); i < size; i++) {
					for(GraphicalElement e : graphicalElements) {
						if (e.coordEquals(erasedCoords.get(i))) {
							e.setDeleted(true);
							e.showEffect();
						}
					}
				}
				draggableEraser.setToStartPosition();
				draggableEraser.toFront();
				setEraseLabel(gameLogic.getErasesLeft());

			}
			catch( Exception e )
			{
				if( e instanceof InterceptFailedException )
				{
					draggableEraser.returnToStartPosition();
				}
			}
		}
		return false;
	}

	private void shuffleGraphicalElement() {

		for (int i = 0; i < gameLogic.getBoard().length; ++i)
			for (int j = 0; j < gameLogic.getBoard()[0].length; ++j)
				for (int k = 0; k < gameLogic.getBoard()[0][0].length; ++k) {

					if (gameLogic.getBoard()[i][j][k] != null) {

						for (int m = 0; m < gameLogic.getTempBoard().length; ++m)
							for (int n = 0; n < gameLogic.getTempBoard()[0].length; ++n)
								for (int l = 0; l < gameLogic.getTempBoard()[0][0].length; ++l) {
									if ( gameLogic.getBoard()[i][j][k] == gameLogic.getTempBoard()[m][n][l]) {
										MoveToAction moveToAction = new MoveToAction();
										moveToAction.setDuration(1);
										moveToAction.setPosition(gameBoard.getFields()[m][n].getX()
																 + gameBoard.getHORIZONTAL_OFFSET() / 2 + gameBoard.getX() / 2,
																 gameBoard.getFields()[m][n].getY() + gameBoard.getY());
										for (GraphicalElement e : graphicalElements)
											if (e.getCoord3().getX() == i && e.getCoord3().getY() == j && e.getCoord3().getZ() == k && e.getColorValue() == (gameLogic.getBoard()[i][j][k].getColorValue())) {
												e.addAction(moveToAction);
												e.setCoord3(new Coord3(m, n, l));
												break;
											}
									}
								}
					}
				}


		for (int x = 0; x < gameLogic.getBoard().length; ++x)
			for (int y = 0; y < gameLogic.getBoard()[0].length; ++y)
				for (int z = 0; z < gameLogic.getBoard()[0].length; ++z) {
					gameLogic.getBoard()[x][y][z] = null;
					gameLogic.getBoard()[x][y][z] = gameLogic.getTempBoard()[x][y][z];
				}


		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				try {
					List<Coord3> updatedCoords = gameLogic.update();
					if (score != gameLogic.getScore()) {
						score = gameLogic.getScore();
						ScaleToAction scaleToAction = new ScaleToAction();
						scaleToAction.setDuration(1.3f);
						scaleToAction.setScale(1.1f);

						scoreLabelContainer.addAction( scaleToAction );
						scoreLabel.setText( String.valueOf( score ) );
						scaleToAction.setReverse( true );
						scoreLabelContainer.addAction( scaleToAction );
					}
					for( Coord3 c : updatedCoords )
					{
						for( GraphicalElement e : graphicalElements )
						{
							if( e.coordEquals( c ) )
							{
								e.setDeleted( true );
								e.showEffect();
							}
						}
					}

					draggableImage1.setCurrentElements( gameLogic.getCurrentElements() );
					draggableImage1.setToStartPosition();
					draggableImage1.toFront();
					gameLogic.setElementDiscardsLeft(gameLogic.getElementDiscardsLeft()+1);
					removeElement();
				} catch (GameOverException e) {
					if(System.currentTimeMillis()-PreferencesHelper.getLastInterstitialAddTimestamp()> 2 * 60 * 1000)
						Game.addsInterface.showInterstitialAdd();
					bus.post(new ReturnToMainScreenEvent(score, true));
				}
			}
		}, 1f);
	}

	private void dragImagesHandler(DraggableImageActionTypeName actionName, DraggableImage draggableImage, float x, float y) {
		switch (actionName) {
			case TOUCH_DOWN: {
				Vector2 position = getViewport().unproject(new Vector2(x, y)); //with viewport
				if (draggableImage.getBounds().contains(position.x, position.y)) {
					draggableImage.setDragged(true);
				}
				break;
			}

			case PAN:
			{
				Vector2 position = getViewport().unproject( new Vector2( x, y ) );
				if( draggableImage.getBounds().contains( position.x, position.y ) && draggableImage1.isDragged() )
				{
					draggableImage.dragImage( position.x, position.y );
				}
				break;
			}
			case PAN_STOP:
				if( draggableImage.isDragged() )
				{
					draggableImage.setDragged( false );
					HashMap<Integer, Element> currentElement = gameLogic.getCurrentElements();
					try
					{

						Coord2 coord = gameBoard.ifIntercept( draggableImage );

						for( Integer level : currentElement.keySet() )
						{
							Group image = new Group();
							int [] elNumber= ElementHelper.getElement(currentElement.get(level).getColorValue());
							Image botImage = new Image(elementAtlas.findRegion("bot" +elNumber[0]+ level));
							Image outline = new Image(new Texture("gameScreenState/singleMiniGame/elements/outline"+level+".png"));
							Image topImage=  new Image(elementAtlas.findRegion("top" +elNumber[1]+ level));
							outline.setSize(ELEMENT_SIZE,ELEMENT_SIZE);
							botImage.setSize(ELEMENT_SIZE,ELEMENT_SIZE);
							topImage.setSize(ELEMENT_SIZE,ELEMENT_SIZE);
							image.addActor(outline);
							image.addActor(botImage);
							image.addActor(topImage);

							GraphicalElement element = new GraphicalElement(image,
																			new Coord3(coord.getX(), coord.getY(), level), currentElement.get(level).getColorValue());
							Color tintingColor = ElementColorHelper.getColor(currentElement.get(level).getColorValue());

							outline.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
							element.setPosition(gameBoard.getFields()[coord.getX()][coord.getY()].getX() + gameBoard.getHORIZONTAL_OFFSET() / 2 + gameBoard.getX() / 2,
												gameBoard.getFields()[coord.getX()][coord.getY()].getY() + gameBoard.getY());
							element.setSize(draggableImage.getWidth(), draggableImage.getHeight());
							graphicalElements.add(element);
							addActor(element);
						}
						List<Coord3> updatedCoords = gameLogic.update();
						if (score != gameLogic.getScore()) {
							score = gameLogic.getScore();
							ScaleToAction scaleToAction = new ScaleToAction();
							scaleToAction.setDuration(1.3f);
							scaleToAction.setScale(1.1f);

							scoreLabelContainer.addAction( scaleToAction );
							scoreLabel.setText( String.valueOf( score ) );
							scaleToAction.setReverse( true );
							scoreLabelContainer.addAction( scaleToAction );
						}
						for( Coord3 c : updatedCoords )
						{
							for( GraphicalElement e : graphicalElements )
							{
								if( e.coordEquals( c ) )
								{
									e.setDeleted( true );
									e.showEffect();
								}
							}
						}
						draggableImage.setCurrentElements( gameLogic.getCurrentElements() );
						draggableImage.setToStartPosition();
						draggableImage.toFront();
					}
					catch( Exception e )
					{
						if( e instanceof InterceptFailedException )
						{
							draggableImage.returnToStartPosition();
						}
						else if( e instanceof GameOverException )
						{

							Timer.schedule(new Timer.Task() {
								@Override
								public void run() {
									gameLogic.saveHighScore(score);
									//       PreferencesHelper.saveMoney(PreferencesHelper.getMoney() + score);
									//GameProgressController.getInstance().addMoney( score / 10 );
									//bus.post( new ShowScoreDialogEvent( score, gameLogic.getPopElements() ) );
									for (GraphicalElement element : graphicalElements) {
										element.setDeleted(true);
									}
									gameLogic.resetGame();
									bus.post(new ReturnToMainScreenEvent(score, true));
									draggableImage1.setToStartPosition();
									draggableImage1.setCurrentElements(gameLogic.getCurrentElements());
									draggableImage1.toFront();
									score = 0;
									scoreLabel.setText(String.valueOf(score));
								}
							}, 0.5f);
						}
					}
				}
				break;
		}
	}

	@Override
	public boolean zoom( float initialDistance, float distance )
	{
		return false;
	}

	@Override
	public boolean pinch( Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1,
						  Vector2 pointer2 )
	{
		return false;
	}

	@Override
	public void pinchStop()
	{

	}


	public void removeElement()
	{
		if( gameLogic.disposeElement()){
			for( Integer level : draggableImage1.getCurrentElements().keySet() )
			{
				Group image = new Group();
				int [] elNumber= ElementHelper.getElement(draggableImage1.getCurrentElements().get(level).getColorValue());
				Image botImage = new Image(elementAtlas.findRegion("bot" +elNumber[0]+ level));
				Image outline = new Image(new Texture("gameScreenState/singleMiniGame/elements/outline"+level+".png"));
				Image topImage=  new Image(elementAtlas.findRegion("top" +elNumber[1]+ level));
				outline.setSize(ELEMENT_SIZE,ELEMENT_SIZE);
				botImage.setSize(ELEMENT_SIZE,ELEMENT_SIZE);
				topImage.setSize(ELEMENT_SIZE,ELEMENT_SIZE);
				image.addActor(outline);
				image.addActor(botImage);
				image.addActor(topImage);

				Color tintingColor = ElementColorHelper.getColor(draggableImage1.getCurrentElements().get(level).getColorValue());
				outline.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
				GraphicalElement element = new GraphicalElement(image,
																new Coord3((int) draggableImage1.getX(), (int) draggableImage1.getY(), level), draggableImage1.getCurrentElements().get(level).getColorValue());

				element.setPosition(draggableImage1.getX(), draggableImage1.getY());
				element.setSize(draggableImage1.getWidth(), draggableImage1.getHeight());
				addActor(element);
				element.setDeleted(true);
			}
			draggableImage1.setCurrentElements(gameLogic.getCurrentElements());}

		setDiscardsLabel(gameLogic.getElementDiscardsLeft());
	}

	private void setEraseLabel(int elementsLeft) {
		String elementsLeftString = String.valueOf(elementsLeft);
		erasesLabel.setText(elementsLeftString);
		if (elementsLeft == 0) {
			Color tintingColor = ElementColorHelper.getLabelColor(true);
			eraserLabelBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
			erasesLabel.setText("Watch Add");
		}
		erasesLabel.pack();
		erasesLabel.setPosition(eraserLabelBackground.getX() + eraserLabelBackground.getWidth() / 2 - erasesLabel.getWidth() / 2,
								eraserLabelBackground.getY() + eraserLabelBackground.getHeight() / 2 - erasesLabel.getHeight() / 2.1f);
	}

	private void setTornadoLabel(int elementsLeft) {
		String elementsLeftString = String.valueOf(elementsLeft);
		tornadoLabel.setText(elementsLeftString);
		if (elementsLeft == 0) {
			Color tintingColor = ElementColorHelper.getLabelColor(true);
			tornadoLabelBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
			tornadoLabel.setText("Watch Add");
		}
		tornadoLabel.pack();
		tornadoLabel.setPosition(tornadoLabelBackground.getX() + tornadoLabelBackground.getWidth() / 2 - tornadoLabel.getWidth() / 2,
								 tornadoLabelBackground.getY() + tornadoLabelBackground.getHeight() / 2 - tornadoLabel.getHeight() / 2.1f);
	}

	private void setDiscardsLabel(int elementsLeft) {
		String elementsLeftString = String.valueOf(elementsLeft);
		discardsLabel.setText(elementsLeftString);
		if (elementsLeft == 0) {
			Color tintingColor = ElementColorHelper.getLabelColor(true);
			trashCounterContainerBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
			discardsLabel.setText("Watch Add");
		}
		discardsLabel.pack();
		discardsLabel.setPosition(trashCounterContainerBackground.getX() + trashCounterContainerBackground.getWidth() / 2 - discardsLabel.getWidth() / 2,
								  trashCounterContainerBackground.getY() + trashCounterContainerBackground.getHeight() / 2 - discardsLabel.getHeight() / 2.1f);
	}

	@Subscribe
	public void showCombo( ShowComboEvent showComboEvent )
	{
		comboContainer.toFront();
		comboLabel.setText( "Combo x" + showComboEvent.getCombo() );
		ScaleToAction scaleToAction = new ScaleToAction();
		scaleToAction.setDuration( 2f );
		scaleToAction.setScale( 1.6f );
		comboContainer.addAction( Actions.sequence( Actions.show(), Actions.parallel( Actions.fadeIn( 2 ), scaleToAction ) ) );
		scaleToAction.setReverse( true );
		comboContainer.addAction( Actions.sequence( Actions.parallel( Actions.fadeOut( 1.3f ), scaleToAction ), Actions.hide() ) );

	}

	@Subscribe
	public void addRewardPoints(AddModificatorsPoints addModificatorsPoints) {
		switch (addModificatorsPoints.getType()) {
			case ERASER:
				gameLogic.setErasesLeft(gameLogic.getErasesLeft() + MODIFICATORS_REVARD);
				setEraseLabel(gameLogic.getErasesLeft());
				Color tintingColor = ElementColorHelper.getLabelColor(false);
				eraserLabelBackground.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f));
				PreferencesHelper.saveEraserModificator(gameLogic.getErasesLeft());
				break;
			case TRASH_CAN:
				gameLogic.setElementDiscardsLeft(gameLogic.getElementDiscardsLeft() + MODIFICATORS_REVARD);
				setDiscardsLabel(gameLogic.getElementDiscardsLeft());
				Color tintingColor1 = ElementColorHelper.getLabelColor(false);
				trashCounterContainerBackground.setColor(Color.WHITE.cpy().lerp(tintingColor1, 1f));
				PreferencesHelper.saveTrashCanModificator(gameLogic.getElementDiscardsLeft());
				break;
			case TORNADO:
				gameLogic.setTornadoLeft(gameLogic.getTornadoLeft() + MODIFICATORS_REVARD);
				setTornadoLabel(gameLogic.getTornadoLeft());
				Color tintingColor2 = ElementColorHelper.getLabelColor(false);
				tornadoLabelBackground.setColor(Color.WHITE.cpy().lerp(tintingColor2, 1f));
				PreferencesHelper.saveTornadoModificator(gameLogic.getTornadoLeft());
				break;
		}
	}
}

