package com.megaapps.einsteingameNoAdds.desktop;

import com.megaapps.einsteingameNoAdds.interfaces.AddsInterface;
import com.megaapps.einsteingameNoAdds.interfaces.HomeButtonInterface;

/**
 * Created by RGrabowskiDev on 04.09.2017.
 */

public class InterfaceBinder implements AddsInterface, HomeButtonInterface
{
    @Override
    public void homeButtonClicked()
    {

    }

    @Override
    public void showBannerAds( boolean show )
    {

    }

    @Override
    public void showRequestVideoAdd( boolean show, ModificatorType type )
    {

    }

    @Override
    public void loadAds()
    {

    }

    @Override
    public void showInterstitialAdd()
    {

    }
}
