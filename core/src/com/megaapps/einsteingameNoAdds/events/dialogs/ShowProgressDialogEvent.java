package com.megaapps.einsteingameNoAdds.events.dialogs;

/**
 * Created by lukasz on 30.05.17.
 */

public class ShowProgressDialogEvent {

    private float bookCount;
    private int coinsCount;
    private int level;
    private float experience;
    private int upperBound;
    private float percFill;
    private long timeLeft;
    private int upgradePointsCount;
    private int bookLimit;
    private int xpPerBook;

    public ShowProgressDialogEvent(float bookCount, int coinsCount, int level, float experience,
                                   int upperBound, float percFill, long timeLeft,
                                   int upgradePointsCount, int bookLimit, int xpPerBook) {
        this.bookCount = bookCount;
        this.coinsCount = coinsCount;
        this.level = level;
        this.experience = experience;
        this.upperBound = upperBound;
        this.percFill = percFill;
        this.timeLeft = timeLeft;
        this.upgradePointsCount = upgradePointsCount;
        this.bookLimit = bookLimit;
        this.xpPerBook = xpPerBook;
    }

    public float getBookCount() {
        return bookCount;
    }

    public int getCoinsCount() {
        return coinsCount;
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public int getLevel() {
        return level;
    }

    public float getExperience() {
        return experience;
    }

    public int getUpperBound() {
        return upperBound;
    }

    public float getPercFill() {
        return percFill;
    }

    public int getUpgradePointsCount() {
        return upgradePointsCount;
    }

    public int getBookLimit() {
        return bookLimit;
    }

    public int getXpPerBook() {
        return xpPerBook;
    }
}
