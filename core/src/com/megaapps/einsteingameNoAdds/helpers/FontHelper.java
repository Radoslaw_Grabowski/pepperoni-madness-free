package com.megaapps.einsteingameNoAdds.helpers;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.fonts.DefaultFont;

/**
 * Created by k on 18.05.2017.
 */

public class FontHelper
{
	private static DefaultFont xxSmallFont;
	private static DefaultFont xSmallFont;
	private static DefaultFont smallFont;
	private static DefaultFont mediumFont;
	private static DefaultFont bigFont;
	private static DefaultFont xBigFont;

	public static void loadFonts()
	{
		xxSmallFont = new DefaultFont( Constants.EXTRA_EXTRA_SMALL_FONT_SIZE );
		xSmallFont = new DefaultFont( Constants.EXTRA_SMALL_FONT_SIZE );
		smallFont = new DefaultFont( Constants.SMALL_FONT_SIZE );
		mediumFont = new DefaultFont( Constants.MEDIUM_FONT_SIZE );
		bigFont = new DefaultFont( Constants.BIG_FONT_SIZE );
		xBigFont = new DefaultFont( Constants.EXTRA_BIG_FONT_SIZE );
	}


	public static BitmapFont getFont( Constants.FontType type )
	{
		switch( type )
		{
			case XX_SMALL:
				if( xxSmallFont == null )
				{
					xxSmallFont = new DefaultFont( Constants.EXTRA_EXTRA_SMALL_FONT_SIZE ); // GAME_HEIGHT / 50
				}
				return xxSmallFont.getFont();
			case X_SMALL:
				if( xSmallFont == null )
				{
					xSmallFont = new DefaultFont( Constants.EXTRA_SMALL_FONT_SIZE ); // GAME_HEIGHT / 35
				}
				return xSmallFont.getFont();
			case SMALL:
				if( smallFont == null )
				{
					smallFont = new DefaultFont( Constants.SMALL_FONT_SIZE ); // GAME_HEIGHT / 25
				}
				return smallFont.getFont();
			case MEDIUM:
				if( mediumFont == null )
					mediumFont = new DefaultFont( Constants.MEDIUM_FONT_SIZE ); // GAME_HEIGHT / 20
				return mediumFont.getFont();
			case LARGE:
				if( bigFont == null )
					bigFont = new DefaultFont( Constants.BIG_FONT_SIZE ); // GAME_HEIGHT / 15
				return bigFont.getFont();
			case X_LARGE:
				if( xBigFont == null )
					xBigFont = new DefaultFont( Constants.EXTRA_BIG_FONT_SIZE ); // GAME_HEIGHT / 10
				return xBigFont.getFont();
			default:
				if( mediumFont == null )
					mediumFont = new DefaultFont( Constants.MEDIUM_FONT_SIZE );
				return mediumFont.getFont();
		}
	}
}
