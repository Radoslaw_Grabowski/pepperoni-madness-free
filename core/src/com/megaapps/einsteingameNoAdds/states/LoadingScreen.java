package com.megaapps.einsteingameNoAdds.states;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;

/**
 * Created by RGrabowskiDev on 30.08.2017.
 */

public class LoadingScreen extends Stage
{
	private Image gameLogo;
	private Label progressTextLabel;
	private boolean isShown = true;

	public LoadingScreen( Batch batch, NewAssetManager assetManager )
	{
		super( new FillViewport( Constants.GAME_SIZE_WIDTH, Constants.GAME_SIZE_HEIGHT ), batch );

		gameLogo = new Image( assetManager.getSync( Constants.GAME_LOGO_PATH, Texture.class ) );
		gameLogo.setSize( Constants.GAME_SIZE_WIDTH, Constants.GAME_SIZE_HEIGHT );
		gameLogo.setPosition( 0, 0 );

		this.addActor( gameLogo );

		// TODO: make loading screen mode for company logo when game starts
		BitmapFont loadingFont = assetManager.getSync( Constants.FONT_PATH, BitmapFont.class );

		Label.LabelStyle style = new Label.LabelStyle( loadingFont, Color.WHITE );
		progressTextLabel = new Label( "Loading 100%", style );
		progressTextLabel.pack();

		progressTextLabel.setPosition( Constants.GAME_SIZE_WIDTH / 2 - progressTextLabel.getWidth() / 2, Constants.GAME_SIZE_HEIGHT / 20 + progressTextLabel.getHeight() );
		this.addActor( progressTextLabel );
	}

	public void show()
	{
		isShown = true;
	}

	public void hide()
	{
		isShown = false;
	}

	public boolean isShown()
	{
		return isShown;
	}

	public void resize( float width, float height )
	{
		getViewport().update( (int) width, (int) height, false );
	}

	public void setLoadProgress( float progress )
	{
		progressTextLabel.setText( "Loading " + (int) ( progress * 100f ) + "%" );
		progressTextLabel.setPosition( Constants.GAME_SIZE_WIDTH / 2 - progressTextLabel.getWidth() / 2, Constants.GAME_SIZE_HEIGHT / 20 + progressTextLabel.getHeight() );
	}
}
