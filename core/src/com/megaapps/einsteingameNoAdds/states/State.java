package com.megaapps.einsteingameNoAdds.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megaapps.einsteingameNoAdds.GameStateManager;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;


/**
 * Created by k on 14.04.2017.
 */

public abstract class State
{

	protected GameStateManager gsm;
	protected NewAssetManager assetManager;
	protected boolean isInitialized = false;

	public State( GameStateManager gsm, NewAssetManager am )
	{
		this.gsm = gsm;
		this.assetManager = am;
	}

	public boolean isInitialized()
	{
		return isInitialized;
	}

	public final NewAssetManager getAsssetManager()
	{
		return assetManager;
	}

	public abstract void initialize();

	public abstract void loadAssets();

	public abstract void update( float dt );

	public abstract void render( SpriteBatch sb );

	public abstract void dispose();

	public abstract void resize( int width, int height );

	public abstract void resume();

	public abstract void pause( boolean pauseMusic );

	public abstract void setInputs();
}
