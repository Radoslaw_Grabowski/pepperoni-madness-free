package com.megaapps.einsteingameNoAdds.helpers;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by Kamil on 6/13/2017.
 */

public class ElementColorHelper {

    public static final int NUMBERS_OF_COLORS = 23;

    public static Color getColor(int value) {

        switch (value) {

            case 0:
                return new Color(255 / 255f, 255 / 255f, 255 / 255f, 1);
            case 1:
                return new Color(81 / 255f, 102 / 255f, 227 / 255f, 1);
            case 2:
                return new Color(255 / 255f, 226 / 255f, 71 / 255f, 1);
            case 3:
                return new Color(177 / 255f, 28 / 255f, 59 / 255f, 1);
            case 4:
                return new Color(86 / 255f, 182 / 255f, 143 / 255f, 1);
            case 5:
                return new Color(171 / 255f, 89 / 255f, 224 / 255f, 1);
            case 6:
                return new Color(222 / 255f, 122 / 255f, 13 / 255f, 1);
            case 7:
                return new Color(0 / 255f, 109 / 255f, 121 / 255f, 1);
            case 8:
                return new Color(109 / 255f, 0 / 255f, 0 / 255f, 1);
            case 9:
                return new Color(96 / 255f, 12 / 255f, 85 / 255f, 1);
            case 10:
                return new Color(129 / 255f, 153 / 255f, 21 / 255f, 1);
            case 11:
                return new Color(25 / 255f, 36 / 255f, 101 / 255f, 1);
            case 12:
                return new Color(104 / 255f, 54 / 255f, 20 / 255f, 1);
            case 13:
                return new Color(174 / 255f, 182 / 255f, 233 / 255f, 1);
            case 14:
                return new Color(232 / 255f, 147 / 255f, 221 / 255f, 1);
            case 15:
                return new Color(133 / 255f, 172 / 255f, 119 / 255f, 1);
            case 16:
                return new Color(146 / 255f, 158 / 255f, 98 / 255f, 1);
            case 17:
                return new Color(111 / 255f, 240 / 255f, 238 / 255f, 1);
            case 18:
                return new Color(22 / 255f, 44 / 255f, 19 / 255f, 1);
            case 19:
                return new Color(235 / 255f, 51 / 255f, 255 / 255f, 1);
            case 20:
                return new Color(67 / 255f, 29 / 255f, 49 / 255f, 1);
            case 21:
                return new Color(38 / 255f, 22 / 255f, 16 / 255f, 1);
            case 22:
                return new Color(157 / 255f, 128 / 255f, 32 / 255f, 1);
            case 23:
                return new Color(0 / 255f, 0 / 255f, 0 / 255f, 1);
            default:
                return new Color(Color.RED);
        }
    }


    public static Color getGreenGameColor() {
        return new Color(54 / 255f, 126 / 255f, 0 / 43, 1);
    }

    public static Color getLabelColor(boolean empty) {

        if (empty)
            return getGreenGameColor();
        else
            return new Color(226 / 255f, 224 / 255f, 226 / 43, 1);

    }

}
