package com.megaapps.einsteingameNoAdds;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.megaapps.einsteingameNoAdds.events.AddModificatorsPoints;
import com.megaapps.einsteingameNoAdds.helpers.MusicHelper;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.interfaces.AddsInterface;
import com.megaapps.einsteingameNoAdds.interfaces.HomeButtonInterface;

import static com.megaapps.einsteingameNoAdds.Game.bus;

public class AndroidLauncher extends AndroidApplication implements AddsInterface, HomeButtonInterface {


	private final static String ANDROID_AD_MOB_BANNER_ID = "ca-app-pub-1157452047737947/8142200718";
	private final static String ANDROID_AD_MOB_REQUEST_VIDEO_ID = "ca-app-pub-1157452047737947/9618933916";

	private final String TAG = "ADS";
	protected AdView adView;
	AdRequest.Builder builder;
	private final int SHOW_BANNER_ADS = 1;
	private final int HIDE_BANNER_ADS = 0;
	private final int SHOW_REQUEST_VIDEO_ADS = 3;
	private final int HIDE_REQUEST_VIDEO_ADS = 4;
	private final int LOAD_REQUEST_ADD = 1;

	private ModificatorType type = ModificatorType.ERASER;


	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;
		RelativeLayout layout = new RelativeLayout(this);
		bus.register(this);

		String appId= "59a66b346059580e02c54424";
		String appSignature ="a67fd343bb0182014fae18c3e67802ef3fec941b";

		Chartboost.startWithAppId(this, appId, appSignature);
		Chartboost.onCreate(this);

		View gameView = initializeForView(new Game(this,this) , config);

		layout.addView(gameView);

		adView = new AdView(this);
		adView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				int visiblity = adView.getVisibility();
				adView.setVisibility(AdView.GONE);
				adView.setVisibility(visiblity);
				Log.i(TAG, "Ad Loaded...");
			}
		});
		adView.setAdSize(AdSize.BANNER);

		//http://www.google.com/admob
		adView.setAdUnitId(ANDROID_AD_MOB_BANNER_ID);

		builder = new AdRequest.Builder();

		//run once before uncommenting the following line. Get TEST device ID from the logcat logs.

		builder.addTestDevice("943741C6D1EA54FDB47E7CF0D1FF15EA");
		builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
		RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT
		);

		layout.addView(adView, adParams);

		setContentView(layout);
		/*generating development Key Hashes */
	/*	PackageInfo info;
		try {
			info = getPackageManager().getPackageInfo("com.megaapps.einsteingame", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md;
				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String something = new String(Base64.encode(md.digest(), 0));
				//String something = new String(Base64.encodeBytes(md.digest()));
				Gdx.app.log("excepFDDFtion", something);
			}
		} catch (PackageManager.NameNotFoundException e1) {
			Gdx.app.log("no package name found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Gdx.app.log("no such algorythm", e.toString());
		} catch (Exception e) {
			Gdx.app.log("exception", e.toString());
		}*/

		Chartboost.setDelegate(new ChartboostDelegate() {

			@Override
			public void didCloseInterstitial( String location )
			{
				PreferencesHelper.saveLastInterstitialTimestamp();
				Chartboost.cacheInterstitial(CBLocation.LOCATION_GAMEOVER  );
			}

			@Override
			public void didFailToLoadRewardedVideo(String location, CBError.CBImpressionError error) {
				super.didFailToLoadRewardedVideo(location, error);
			}

			@Override
			public void didDismissRewardedVideo(String location) {
				super.didDismissRewardedVideo(location);
			}

			@Override
			public void didCloseRewardedVideo(String location) {
				if (!PreferencesHelper.getMusicMute())
					MusicHelper.getThemeMusic().play();
				super.didCloseRewardedVideo(location);

			}

			@Override
			public void didClickRewardedVideo(String location) {
				super.didClickRewardedVideo(location);
			}

			@Override
			public void didCompleteRewardedVideo(String location, int reward) {
				bus.post(new AddModificatorsPoints(type));
				super.didCompleteRewardedVideo(location, reward);
			}

			@Override
			public void didDisplayRewardedVideo(String location) {
				super.didDisplayRewardedVideo(location);
			}

			@Override
			public void willDisplayVideo(String location) {
				if (MusicHelper.getThemeMusic().isPlaying())
					MusicHelper.getThemeMusic().pause();
				super.willDisplayVideo(location);
			}

			@Override
			public void didInitialize() {
				super.didInitialize();
			}
		});

		Chartboost.cacheRewardedVideo(CBLocation.LOCATION_GAMEOVER);

		AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(),
				"858295787", "06YUCI3OrHUQ65uimQM", "0.00", false);
	}
	@Override
	protected void onStart() {
		Chartboost.onStart(this);
		super.onStart();
	}

	@Override
	protected void onPause() {
		Chartboost.onPause(this);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Chartboost.onResume(this);
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		Chartboost.onDestroy(this);
		super.onDestroy();
	}

	@Override
	public void showBannerAds(boolean show) {
		handler.sendEmptyMessage(show ? SHOW_BANNER_ADS : HIDE_BANNER_ADS);
	}

	@Override
	public void showRequestVideoAdd(boolean show, ModificatorType type) {
		this.type = type;
		Chartboost.showRewardedVideo(CBLocation.LOCATION_GAMEOVER);

	}

	@Override
	public void loadAds() {
		Chartboost.cacheInterstitial(CBLocation.LOCATION_GAMEOVER  );
		loadAdd.sendEmptyMessage(LOAD_REQUEST_ADD);
	}

	@Override
	public void showInterstitialAdd()
	{
		Chartboost.showInterstitial(CBLocation.LOCATION_GAMEOVER  );
	}

	protected Handler loadAdd = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case LOAD_REQUEST_ADD: {
					adView.loadAd(builder.build());
					break;
				}
			}
		}
	};


	protected Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case SHOW_BANNER_ADS: {
					adView.setVisibility(View.VISIBLE);
					break;
				}
				case HIDE_BANNER_ADS: {
					adView.setVisibility(View.GONE);
					break;
				}
				case SHOW_REQUEST_VIDEO_ADS: {

					break;
				}
				case HIDE_REQUEST_VIDEO_ADS: {
					adView.setVisibility(View.GONE);
					break;
				}
			}
		}

	};


	@Override
	public void homeButtonClicked() {
		moveTaskToBack(true);
	}

}
