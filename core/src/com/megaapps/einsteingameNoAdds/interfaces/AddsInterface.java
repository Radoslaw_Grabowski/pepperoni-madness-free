package com.megaapps.einsteingameNoAdds.interfaces;

/**
 * Created by k on 02.06.2017.
 */

public interface AddsInterface {

    public enum ModificatorType {
        ERASER, TORNADO, TRASH_CAN
    }
    public void showBannerAds(boolean show);

    public void showRequestVideoAdd(boolean show, ModificatorType type);

    public void loadAds();
    public void showInterstitialAdd();
}
