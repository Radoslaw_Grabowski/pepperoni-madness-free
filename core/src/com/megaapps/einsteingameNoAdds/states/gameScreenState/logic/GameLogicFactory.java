package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;


import com.megaapps.einsteingameNoAdds.Constants;

/**
 * Created by lukasz on 04.05.17.
 */

public class GameLogicFactory {

    public GameLogic create(Constants.GameDifficulty difficulty) {
        switch (difficulty) {
            case EASY:
                return new com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.GameLogicEasy();
            case MEDIUM:
                return new GameLogicMedium();
            case HARD:
                return new com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.GameLogicHard();
            default:
                return new com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.GameLogicHard();
        }
    }
}
