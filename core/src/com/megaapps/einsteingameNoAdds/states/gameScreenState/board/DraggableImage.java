package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.megaapps.einsteingameNoAdds.helpers.ElementColorHelper;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Element;

import java.util.HashMap;

import static com.megaapps.einsteingameNoAdds.Constants.ELEMENT_SIZE;

/**
 * Created by lukasz on 02.05.17.
 */

public class DraggableImage extends Group implements Draggable
{

	TextureAtlas atlas;
	private Rectangle boundingRectangle, slotRectangle;
	private boolean isDragged;
	private Vector2 startPosition;
	private HashMap<Integer, Element> currentElements;

	public DraggableImage( HashMap<Integer, Element> elements, NewAssetManager assetManager )
	{


		atlas = assetManager.get( "gameScreenState/singleMiniGame/elements/pizzaElements.atlas", TextureAtlas.class );

		setSize( ELEMENT_SIZE, ELEMENT_SIZE );

		setCurrentElements( elements );
	}

	public void setPositionAndSetupBounds( int x, int y )
	{
		setPosition( x, y, Align.center );
		startPosition = new Vector2( getX(), getY() );
		boundingRectangle = new Rectangle( getX(), getY(), getWidth() * 1.5f, getHeight() * 1.5f );
		slotRectangle = new Rectangle( getX() + getWidth() / 2 - boundingRectangle.getWidth() / 4, getY() + getHeight() / 2 - boundingRectangle.getHeight() / 4, boundingRectangle.getWidth() / 2, boundingRectangle.getHeight() / 2 );
		boundingRectangle.setPosition( getX() + ELEMENT_SIZE / 2 - boundingRectangle.getWidth() / 2,
									   getY() + ELEMENT_SIZE / 2 - boundingRectangle.getHeight() / 2 );
		slotRectangle.setPosition( getX() + getWidth() / 2 - slotRectangle.getWidth() / 2, getY() + getHeight() / 2 - slotRectangle.getHeight() / 2 );
	}

	public HashMap<Integer, Element> getCurrentElements()
	{
		return currentElements;
	}

	public void setCurrentElements( HashMap<Integer, Element> elements )
	{
		this.currentElements = elements;
		clear();
		for( Integer level : elements.keySet() )
		{
			Element element = currentElements.get( level );
			Group image = new Group();
			int[] elNumber = ElementHelper.getElement( element.getColorValue() );
			Image botImage = new Image( atlas.findRegion( "bot" + elNumber[0] + level ) );
			Image outline = new Image( new Texture( "gameScreenState/singleMiniGame/elements/outline" + level + ".png" ) );
			Image topImage = new Image( atlas.findRegion( "top" + elNumber[1] + level ) );
			outline.setSize( ELEMENT_SIZE, ELEMENT_SIZE );
			botImage.setSize( ELEMENT_SIZE, ELEMENT_SIZE );
			topImage.setSize( ELEMENT_SIZE, ELEMENT_SIZE );

			image.addActor( outline );
			image.addActor( botImage );
			image.addActor( topImage );
			Color tintingColor = ElementColorHelper.getColor( element.getColorValue() );
			outline.setColor( Color.WHITE.cpy().lerp( tintingColor, 1f ) );

			image.setSize( ELEMENT_SIZE, ELEMENT_SIZE );
			addActor( image );
		}
	}

	public void setToStartPosition()
	{
		setPosition( startPosition.x, startPosition.y );
		boundingRectangle.setPosition( getX() + ELEMENT_SIZE / 2 - boundingRectangle.getWidth() / 2,
									   getY() + ELEMENT_SIZE / 2 - boundingRectangle.getHeight() / 2 );
		slotRectangle.setPosition( getX() + getWidth() / 2 - slotRectangle.getWidth() / 2, getY() + getHeight() / 2 - slotRectangle.getHeight() / 2 );
	}

	public boolean isDragged()
	{
		return isDragged;
	}

	public void setDragged( boolean dragged )
	{
		isDragged = dragged;
	}

	public void dragImage( float x, float y )
	{
		setPosition( x - getWidth() / 2, y - getHeight() / 2 );
		boundingRectangle.setPosition( getX() + ELEMENT_SIZE / 2 - boundingRectangle.getWidth() / 2,
									   getY() + ELEMENT_SIZE / 2 - boundingRectangle.getHeight() / 2 );
		slotRectangle.setPosition( getX() + getWidth() / 2 - slotRectangle.getWidth() / 2, getY() + getHeight() / 2 - slotRectangle.getHeight() / 2 );
	}

	public Rectangle getBounds()
	{
		return boundingRectangle;
	}

	public Rectangle getSlotRectangle()
	{
		return slotRectangle;
	}

	public void update()
	{

	}

	@Override
	public void draw( Batch batch, float parentAlpha )
	{
		super.draw( batch, parentAlpha );
	}

	public Vector2 getStartPosition()
	{
		return startPosition;
	}

	public void returnToStartPosition()
	{
		MoveToAction moveToAction = new MoveToAction();
		moveToAction.setPosition( getStartPosition().x, getStartPosition().y );
		boundingRectangle.setPosition( getStartPosition().x + ELEMENT_SIZE / 2 - boundingRectangle.getWidth() / 2,
									   getStartPosition().y + ELEMENT_SIZE / 2 - boundingRectangle.getHeight() / 2 );
		slotRectangle.setPosition( getStartPosition().x + getWidth() / 2 - slotRectangle.getWidth() / 2, getStartPosition().y + getHeight() / 2 - slotRectangle.getHeight() / 2 );
		moveToAction.setDuration( 0.5f );
		addAction( moveToAction );

	}
}
