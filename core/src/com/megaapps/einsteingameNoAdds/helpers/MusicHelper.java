package com.megaapps.einsteingameNoAdds.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by k on 30.05.2017.
 */

public class MusicHelper
{
	private static final String CLICK_SOUND_PATH = "sounds/buttonPressed.wav";
	private static final String THEME_MUSIC_PATH = "sounds/main.mp3";
	private static final String MINI_GAME_MUSIC_PATH = "sounds/game.mp3";
	private static final String REMOVE_PIZZA_PATH = "sounds/removePizza.wav";

	private static Sound clickSound, removePizza;
	private static Music themeMusic, miniGameMusic;

	public static void removePizzaSound()
	{
		if( !PreferencesHelper.getSoundMute() )
			getRemovePizzaInstance().play();
	}


	private static Sound getRemovePizzaInstance()
	{
		if( removePizza == null )
			removePizza = Gdx.audio.newSound( Gdx.files.internal( REMOVE_PIZZA_PATH ) );

		return removePizza;
	}

	public static void playClickSound()
	{
		if( !PreferencesHelper.getSoundMute() )
			getClickSound().play();
	}

	public static Sound getClickSound()
	{
		return getClickSoundInstance();
	}

	public static Music getThemeMusic()
	{
		return getThemeMusicInstance();
	}

//	public static Music getMiniGameMusic()
//	{
//		return getMiniGameMusicInstance();
//	}


//	public static Music getMiniGameMusicInstance()
//	{
//		if( miniGameMusic == null )
//		{
//			// TODO:
//			miniGameMusic = Gdx.audio.newMusic( Gdx.files.internal( MINI_GAME_MUSIC_PATH ) );
//			miniGameMusic.setLooping( true );
//		}
//
//		return miniGameMusic;
//
//	}

	private static Music getThemeMusicInstance()
	{

		if( themeMusic == null )
		{
			themeMusic = Gdx.audio.newMusic( Gdx.files.internal( THEME_MUSIC_PATH ) );
			themeMusic.setLooping( true );
		}

		return themeMusic;
	}


	private static Sound getClickSoundInstance()
	{
		if( clickSound == null )
			clickSound = Gdx.audio.newSound( Gdx.files.internal( CLICK_SOUND_PATH ) );

		return clickSound;
	}


}
