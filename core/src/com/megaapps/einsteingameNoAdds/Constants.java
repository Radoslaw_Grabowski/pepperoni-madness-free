package com.megaapps.einsteingameNoAdds;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by k on 20.04.2017.
 */

public class Constants
{
	////////////////////////////////////////////////////////////////////////////////////////////////

	public static final boolean DEBUG_MODE = false;
	public static final boolean ALWAYS_SHOW_TUTORIAL = false;

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Assets
	public static final String FONT_XX_SMALL_PATH													 = "styles/gameFontXX_SMALL.ttf";
	public static final String FONT_PATH 												             = "styles/gameFont.ttf";
	public static final String GAME_SCREEN_STATE_ERASER_PATH                                         = "gameScreenState/eraser.png";
	public static final String GAME_LOGO_PATH 											             = "logo.jpg";
	public static final String GAME_SCREEN_STATE_MODIFIER_BACKGROUND_PATH                            = "gameScreenState/modifierBackground.png";
	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_BACKGROUND_PATH                    = "gameScreenState/singleMiniGame/background.png";
	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_BLACK_LABEL_PATH                   = "gameScreenState/singleMiniGame/blackLabel.png";
	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_BOTTOM_LABEL_PATH                  = "gameScreenState/singleMiniGame/bottomLabel.png";
	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_TRASH_CAN_PATH                     = "gameScreenState/singleMiniGame/trashCan.png";
	public static final String GAME_SCREEN_STATE_TORNADO_PATH                                        = "gameScreenState/tornado.png";
	public static final String MINI_GAME_TUTORIAL_STATE_GAME_TUTORIAL_PATH                           = "miniGameTutorialState/gameTutorial.jpg";
	public static final String PIZZA_GAME_TUTORIAL_TUT1_PATH                                         = "pizzaGameTutorial/tut1.jpg";
	public static final String PIZZA_GAME_TUTORIAL_TUT2_PATH                                         = "pizzaGameTutorial/tut2.jpg";
	public static final String PIZZA_GAME_TUTORIAL_TUT3_PATH                                         = "pizzaGameTutorial/tut3.jpg";

	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_ELEMENTS_ATLAS_PATH                = "gameScreenState/singleMiniGame/elements.atlas";
	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_ELEMENTS_PIZZA_ELEMENTS_ATLAS_PATH = "gameScreenState/singleMiniGame/elements/pizzaElements.atlas";
	public static final String GAME_SCREEN_STATE_SINGLE_MINI_GAME_PIZZA_MENU_ATLAS_PATH              = "gameScreenState/singleMiniGame/pizzaMenu.atlas";

	public static final String CLICK_SOUND_PATH 													 = "sounds/buttonPressed.wav";
	public static final String THEME_MUSIC_PATH 												     = "sounds/main.mp3";
	public static final String REMOVE_PIZZA_PATH 													 = "sounds/removePizza.wav";

	////////////////////////////////////////////////////////////////////////////////////////////////

	public static final float GAME_SIZE_WIDTH = 1080;//480;
	public static final float GAME_SIZE_HEIGHT = 1920;//800;

	////////////////////////////////////////////////////////////////////////////////////////////////

	public enum FontType
	{
		XX_SMALL, X_SMALL, SMALL, MEDIUM, LARGE, X_LARGE
	}

	// Font size
	public static final int EXTRA_EXTRA_SMALL_FONT_SIZE = ( int ) ( GAME_SIZE_HEIGHT / 50 );
	public static final int EXTRA_SMALL_FONT_SIZE = ( int ) ( GAME_SIZE_HEIGHT / 35 );
	public static final int SMALL_FONT_SIZE = ( int ) ( GAME_SIZE_HEIGHT / 25 );
	public static final int MEDIUM_FONT_SIZE = ( int ) ( GAME_SIZE_HEIGHT / 20 );
	public static final int BIG_FONT_SIZE = ( int ) ( GAME_SIZE_HEIGHT / 15 );
	public static final int EXTRA_BIG_FONT_SIZE = ( int ) ( GAME_SIZE_HEIGHT / 10 );

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Game Board Constants
	public static final float GAME_BOARD_PADDING = GAME_SIZE_WIDTH * 0.025f;
	public static final float ELEMENT_SIZE = GAME_SIZE_HEIGHT / 9;
	public static final float BOTTOM_TAB_HEIGHT = GAME_SIZE_HEIGHT / 2.75f;
	public static final float MODIFIER_SIZE = GAME_SIZE_HEIGHT / 10;

	//Minigame Tutorial Constants
	public static final float MINIGAME_BOTTOM_LABEL_BLACK = GAME_SIZE_HEIGHT / 5.4f;
	public static final float MINIGAME_BOTTOM_LABEL_BLACK_HEIGHT = ELEMENT_SIZE * 1.1f;

	// Default clear colors
	public static final Vector3 DEFAULT_CLEAR_COLORS = new Vector3( 37 / 255f, 16 / 255f, 4 / 255f );
	public static final Vector3 TUTORIAL_CLEAR_COLORS = new Vector3( 18 / 255f, 7 / 255f, 4 / 255f );

	//ADS_HEIGHT
	public static int ADD_HEIGHT = 50;

	public enum GameDifficulty
	{
		EASY, MEDIUM, HARD
	}
}
