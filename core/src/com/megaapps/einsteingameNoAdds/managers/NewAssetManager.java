package com.megaapps.einsteingameNoAdds.managers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.megaapps.einsteingameNoAdds.Constants;

/**
 * Created by RGrabowskiDev on 30.08.2017.
 */

public class NewAssetManager
{
	private AssetManager am;

	public NewAssetManager()
	{
		am = new AssetManager();

		Gdx.app.log( "NewAssetManager", "creating assetManager Instance" );

		FileHandleResolver resolver = new InternalFileHandleResolver();
		am.setLoader( FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader( resolver ) );
		am.setLoader( BitmapFont.class, ".ttf", new FreetypeFontLoader( resolver ) );

		Texture.setAssetManager( am );
	}

	public boolean isAsyncLoadFinished()
	{
		return am.update();
	}

	public <T> void loadAsync( /*String callingClass, */String fileName, Class<T> classType )
	{
		if( !am.isLoaded( fileName ) )
		{
			if( Constants.DEBUG_MODE && !Gdx.files.internal( fileName ).exists() )
			{
				Gdx.app.error( "NewAssetManager", "File \"" + fileName + "\" doeas not exist!." );
				return;
			}

			//Gdx.app.log( "NewAssetManager", "AsyncLoad OK in: " + callingClass + " loading asset: " + fileName );
			// Font async loading
			if( classType == BitmapFont.class )
			{
				FreetypeFontLoader.FreeTypeFontLoaderParameter loaderParam = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
				loaderParam.fontFileName = fileName;
				loaderParam.fontParameters.size = resolveFontSize( fileName );

				am.load( fileName, BitmapFont.class, loaderParam );
				Gdx.app.log( "NewAssetManager", "Queueing font loading, patch: \"" + fileName + "\" - OK" );

				return;
			}

			am.load( fileName, classType );
			Gdx.app.log( "NewAssetManager", "Queueing asset loading, type: " + classType.getSimpleName() + ", patch: \"" + fileName + "\" - OK" );
			return;
		}

		//Gdx.app.log( "NewAssetManager", "AsyncLoad FAIL in: " + callingClass + "  duplicate loading asset: " + fileName );
	}

	public <T> T get( /*String callingClass, */String fileName, Class<T> classType )
	{
		// Other asset async loading
		if( am.isLoaded( fileName ) )
		{
			//Gdx.app.log( "NewAssetManager", "Get asset in: " + callingClass + " asset: " + fileName );
			return classType.cast( am.get( fileName, classType ) );
		}
		else
		{
			Gdx.app.error( "NewAssetManager", "Asset " + fileName + " was not asynchronously loaded!!" );
			return null;
		}
	}

	public <T> T getSync( /*String callingClass, */String fileName, Class<T> classType )
	{
		//Gdx.app.log( "NewAssetManager", "GetSyns called in: " + callingClass + " loading asset: " + fileName );

		if( am.isLoaded( fileName, classType ) )
			return am.get( fileName, classType );

		// Font sunchronous loading
		if( classType == BitmapFont.class )
		{
			FreetypeFontLoader.FreeTypeFontLoaderParameter loaderParam = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
			loaderParam.fontFileName = fileName;
			loaderParam.fontParameters.size = resolveFontSize( fileName );

			am.load( fileName, BitmapFont.class, loaderParam );
			am.finishLoadingAsset( fileName );
		}
		else
		{
			am.load( fileName, classType );
			am.finishLoadingAsset( fileName );
		}

		return am.get( fileName, classType );
	}

	private int resolveFontSize( String fontName )
	{
		if( fontName == Constants.FONT_PATH )
			return Constants.SMALL_FONT_SIZE;
	   if( fontName == Constants.FONT_XX_SMALL_PATH )
			return Constants.EXTRA_EXTRA_SMALL_FONT_SIZE;

		return Constants.SMALL_FONT_SIZE;
	}

	public float getLoadProgress()
	{
		return am.getProgress();
	}

	public void resume()
	{
		// Reload managed assets on resume
		if( Gdx.app.getType() == Application.ApplicationType.Android )
			Texture.setAssetManager( am );
	}

	public void dispose()
	{
		am.dispose();
	}
}
