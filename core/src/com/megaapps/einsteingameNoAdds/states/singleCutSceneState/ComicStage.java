package com.megaapps.einsteingameNoAdds.states.singleCutSceneState;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.megaapps.einsteingameNoAdds.Constants;

/**
 * Created by lukasz on 19.05.17.
 */

public class ComicStage extends Stage
{

	private OrthographicCamera camera;

	public ComicStage( int id )
	{
		setViewport( new ExtendViewport( Constants.GAME_SIZE_WIDTH, Constants.GAME_SIZE_HEIGHT ) );
		camera = new OrthographicCamera( getViewport().getScreenWidth(), getViewport().getScreenHeight() );
		getViewport().setCamera( camera );
		getCamera().position.set( Constants.GAME_SIZE_WIDTH / 2, Constants.GAME_SIZE_HEIGHT / 2, 0 );
		Image comic = new Image( new Texture( "cutSceneState/scenes/cutscenka1.jpg" ) );
		addActor( comic );
	}
}
