package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by lukasz on 09.06.17.
 */

public interface Draggable {

    public boolean isDragged();
    public void setDragged(boolean dragged);
    public void dragImage(float x, float y);
    public Rectangle getBounds();
    public Rectangle getSlotRectangle();
    public Vector2 getStartPosition();
    public void returnToStartPosition();
    public void setToStartPosition();

}
