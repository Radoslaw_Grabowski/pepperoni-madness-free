package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Random;

/**
 * Created by lukasz on 05.05.17.
 */

public class GraphicalElement extends Group {

    private static final float GRAVITY = -1f;
    private boolean deleted,showEffect;
    private com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord3 coord3;
    private Vector2 velocity;
    private ParticleEffect particleEffect;
    private int colorValue;

    public GraphicalElement(Group region, com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord3 coord3, int colorValue) {
        addActor(region);
        deleted = false;
        velocity = new Vector2(0, 0);
        this.coord3 = coord3;
        this.colorValue = colorValue;
        initEffect();

    }

    public int getColorValue() {
        return colorValue;
    }

    private void initEffect() {
        Random rand = new Random();
        int randColor= rand.nextInt(4);
        particleEffect= new ParticleEffect();
        switch (randColor){

            case 0:
                particleEffect.load(Gdx.files.internal("effects/miniGameEffectViolet.effect"), Gdx.files.internal("effects"));
                break;
            case 1:
                particleEffect.load(Gdx.files.internal("effects/miniGameEffectRed.effect"), Gdx.files.internal("effects"));
                break;
            case 2:
                particleEffect.load(Gdx.files.internal("effects/miniGameEffectBlue.effect"), Gdx.files.internal("effects"));
                break;
            case 3:
                particleEffect.load(Gdx.files.internal("effects/miniGameEffectGreen.effect"), Gdx.files.internal("effects"));
             break;
                default:
                particleEffect.load(Gdx.files.internal("effects/miniGameEffectViolet.effect"), Gdx.files.internal("effects"));
                    break;
        }
    }

    public void setCoord3(com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord3 coord3) {
        this.coord3 = coord3;
    }

    public com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord3 getCoord3() {
        return coord3;
    }

    public void showEffect(){
        showEffect=true;
        particleEffect.setPosition(getX()+getWidth()/2,getY()+getHeight()/2);
        particleEffect.start();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (deleted) {
            velocity.add(0,GRAVITY);
            setPosition(getX() + velocity.x, getY() + velocity.y);


        }
        if(showEffect){
            particleEffect.update(delta);
        if(particleEffect.isComplete())
            particleEffect.dispose();}
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if(showEffect)
        particleEffect.draw(batch);
    }

    public void setDeleted(boolean deleted) {

        Random random = new Random(TimeUtils.nanoTime());
        this.velocity.y = random.nextFloat() * 20f;
        this.velocity.x = random.nextFloat() * 10f - 5f;
        this.deleted = deleted;
    }

    public boolean coordEquals(com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord3 coord) {
        return coord.equals(coord3);
    }
}
