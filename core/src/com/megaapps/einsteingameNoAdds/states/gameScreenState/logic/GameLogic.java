package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;

import com.badlogic.gdx.utils.Json;
import com.megaapps.einsteingameNoAdds.Game;
import com.megaapps.einsteingameNoAdds.events.ShowComboEvent;
import com.megaapps.einsteingameNoAdds.exceptions.GameOverException;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by lukasz on 04.05.17.
 */

abstract public class GameLogic {

    public static final String TAG = GameLogic.class.getSimpleName();

    protected static final int PER_SUCCESS_POINTS = 5;
    protected static final int INCREASE_ELEMENT_COLOR_RANGE_INTERVAL=50;
    protected static final int START_NUMBER_OF_ELEMENT_TYPES=3;
    private static final int MAX_ELEMENTS_TYPES =14 ;
    protected static  int NUMBER_OF_ELEMENT_TYPES = START_NUMBER_OF_ELEMENT_TYPES;
    protected static final int DIMENSIONALITY = 3;
    protected int score = 0,popElements=0,combo;
    protected int elementDiscardsLeft = PreferencesHelper.getTrashCanModidificator();
    private int erasesLeft = PreferencesHelper.getEraserModidificator();
    private int tornadoLeft = PreferencesHelper.getTornadoModidificator();
    private int highScore = 0;

    public int getTornadoLeft() {
        return tornadoLeft;
    }

    public void setTornadoLeft(int tornadoLeft) {
        this.tornadoLeft = tornadoLeft;
    }

    protected Element[][][] board = new Element[DIMENSIONALITY][DIMENSIONALITY][DIMENSIONALITY];
    protected HashMap<Integer, Element> currentElements = new HashMap<Integer, Element>();
    protected Element[][][] tempBoard;

    public Element[][][] getTempBoard() {
        return tempBoard;
    }

    public void setElementDiscardsLeft(int elementDiscardsLeft) {
        this.elementDiscardsLeft = elementDiscardsLeft;
    }

    GameLogic() {
        Game.bus.register(this);
        setHighScore();

        try {
            currentElements = generateCurrentElements();
        } catch (GameOverException e) {
            e.printStackTrace();
        }
    }

    public Element[][][] getBoard() {
        return board;
    }

    public void resetGame() {
        for (int i = 0; i < DIMENSIONALITY; i++) {
            for (int j = 0; j < DIMENSIONALITY; j++) {
                for (int k = 0; k < DIMENSIONALITY; k++) {
                    board[i][j][k] = null;
                }
            }
        }
        score = 0;

        try {
            currentElements = generateCurrentElements();
        } catch (GameOverException e) {
            e.printStackTrace();
        }

    }

    public boolean disposeElement() {
        if (elementDiscardsLeft > 0) {
            try {
                currentElements = generateCurrentElements();
                elementDiscardsLeft--;
                PreferencesHelper.saveTrashCanModificator(elementDiscardsLeft);
            } catch (GameOverException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    public int getElementDiscardsLeft() {
        return elementDiscardsLeft;
    }

    public int getScore() {
        return score;
    }

    public int getErasesLeft() {
        return erasesLeft;
    }

    public void setErasesLeft(int erasesLeft) {
        this.erasesLeft = erasesLeft;
    }

    public boolean decrementErases() {
        if (erasesLeft > 0) {
            erasesLeft--;
          PreferencesHelper.saveEraserModificator(erasesLeft);
            return true;
        } else {
            return false;
        }
    }

    public HashMap<Integer, Element> getCurrentElements() {
        return currentElements;
    }

    public boolean checkIfEmpty(int x, int y) {
        for (int i = 0; i < 3; ++i) {
            if (board[x][y][i] != null)
                return false;
        }
        return true;
    }

    public boolean setValue(int x, int y) /* throws GameOverException */ {
        for (Integer level : currentElements.keySet()) {
            if (board[x][y][level] != null)
                return false;
        }
        for (Integer level : currentElements.keySet()) {
            board[x][y][level] = currentElements.get(level);
        }
        //currentElements = generateCurrentElements();
        return true;
    }

    protected HashMap<Integer, Element> generateCurrentElements() throws com.megaapps.einsteingameNoAdds.exceptions.GameOverException {
        Set<Set<Integer>> availableLevels = new HashSet<Set<Integer>>();
        Set<Integer> ALL_LEVELS = new HashSet<Integer>();
        ALL_LEVELS.addAll(Arrays.asList(0, 1, 2));
        Set<Integer> LEVELS_0_1 = new HashSet<Integer>();
        LEVELS_0_1.addAll(Arrays.asList(0, 1));
        Set<Integer> LEVELS_0_2 = new HashSet<Integer>();
        LEVELS_0_2.addAll(Arrays.asList(0, 2));
        Set<Integer> LEVELS_1_2 = new HashSet<Integer>();
        LEVELS_1_2.addAll(Arrays.asList(1, 2));
        List<Set<Integer>> ALL_PAIRS = new ArrayList<Set<Integer>>(3);
        ALL_PAIRS.add(LEVELS_0_1);
        ALL_PAIRS.add(LEVELS_0_2);
        ALL_PAIRS.add(LEVELS_1_2);
        levelsLoop:
        for (int i = 0; i < DIMENSIONALITY; i++) {
            for (int j = 0; j < DIMENSIONALITY; j++) {
                Set<Integer> levels = new HashSet<Integer>();
                for (int k = 0; k < DIMENSIONALITY; k++) {
                    if (board[i][j][k] == null) {
                        levels.add(k);
                    }
                }
                if (!levels.isEmpty())
                    availableLevels.add(levels);
                if (availableLevels.contains(ALL_LEVELS) || (availableLevels.containsAll(ALL_PAIRS))) {
                    break levelsLoop;
                }
            }
        }
        HashMap<Integer, Element> currentElements = new HashMap<Integer, Element>();
        Random rand = new Random(System.currentTimeMillis());
        if (availableLevels.contains(ALL_LEVELS) || (availableLevels.containsAll(ALL_PAIRS))) {
            for(int i = 0; i < 2; i++){
                currentElements.put(rand.nextInt(DIMENSIONALITY),
                        new Element(rand.nextInt(NUMBER_OF_ELEMENT_TYPES)));
            }
        } else {
            if (availableLevels.size() == 0) {
                throw new GameOverException();
            }
            Set<Integer> levels = (Set<Integer>) availableLevels.toArray()[rand.nextInt(availableLevels.size())];
            for (Integer level : levels) {
                currentElements.put(level, new Element(rand.nextInt(NUMBER_OF_ELEMENT_TYPES)));
            }
        }
        return currentElements;
    }

 /*   public boolean discardCurrentElement() {
        if (elementDiscardsLeft > 0) {
            try {
                currentElements = generateCurrentElements();
            } catch (GameOverException e) {
                e.printStackTrace();
            }
            elementDiscardsLeft--;
            Gdx.app.log(TAG, "element discards left " + elementDiscardsLeft);
            return true;
        } else {
            return false;
        }
    }*/

    abstract public List<Coord3> update() throws com.megaapps.einsteingameNoAdds.exceptions.GameOverException;

    public void shuffleLogicBoards() {


        tempBoard = new Element[DIMENSIONALITY][DIMENSIONALITY][DIMENSIONALITY];

        List<Element> firstLevel = new ArrayList<Element>();
        List<Element> secondLevel = new ArrayList<Element>();
        List<Element> thirdLevel = new ArrayList<Element>();
        firstLevel.clear();
        secondLevel.clear();
        thirdLevel.clear();

        for (int i = 0; i < board.length; i++)
            for (int j = 0; j < board[0].length; j++) {
                firstLevel.add(board[i][j][0]);
                secondLevel.add(board[i][j][1]);
                thirdLevel.add(board[i][j][2]);
            }

        Collections.shuffle(firstLevel);
        Collections.shuffle(secondLevel);
        Collections.shuffle(thirdLevel);
        for (int m = 0; m < board.length; m++)
            for (int n = 0; n < board[0].length; n++) {
                tempBoard[m][n][0] = firstLevel.get((m * board.length) + n);
                tempBoard[m][n][1] = secondLevel.get((m * board.length) + n);
                tempBoard[m][n][2] = thirdLevel.get((m * board.length) + n);
            }
    }

    public List<Coord3> erase(Coord2 coord) {
        int x, y;
        x = coord.getX();
        y = coord.getY();
        board[x][y][0] = null;
        board[x][y][1] = null;
        board[x][y][2] = null;
        return Arrays.asList(new Coord3(x, y, 0), new Coord3(x, y, 1), new Coord3(x, y, 2));
    }

    public void saveHighScore(int score) {
        int[] highScores = PreferencesHelper.getHighScore();
        boolean isHighScoreChanged = false;
        for (int i = 0; i < highScores.length; ++i) {
            if (highScores[i] < score) {
                highScores[i] = score;
                isHighScoreChanged = true;
                break;
            }
        }
        if (isHighScoreChanged) {
            Hashtable<String, String> hashTable = new Hashtable<String, String>();

            Json json = new Json();
            hashTable.put("highscore", json.toJson(highScores));
            PreferencesHelper.saveHighScores(hashTable);
            setHighScore();
        }
    }

    private void setHighScore() {
        highScore = PreferencesHelper.getHighScore()[0];
    }

    public int getHighScore() {
        return highScore;
    }

    public int getPopElements() {
        return popElements;
    }
    public void showCombo(){
        if(combo>1){
            Game.bus.post(new ShowComboEvent(combo));
        }
    }
    public void increaseElementColorRange(int score){

        if(score>=700){
            NUMBER_OF_ELEMENT_TYPES=MAX_ELEMENTS_TYPES;
        }else{
        score/=INCREASE_ELEMENT_COLOR_RANGE_INTERVAL;

        NUMBER_OF_ELEMENT_TYPES=START_NUMBER_OF_ELEMENT_TYPES+score;}
    }
}
