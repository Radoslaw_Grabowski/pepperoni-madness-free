package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Kamil on 7/5/2017.
 */

public class ElementHelper {

    public static Integer[]  top = {0,1,2,3,4};
    public static Integer[] bottom = {0,1,2,3};

            public static void shuffleTables(){
                List<Integer> tempTop= Arrays.asList(top);
                List<Integer> tempBottom= Arrays.asList(bottom);
                Collections.shuffle(tempTop);
                Collections.shuffle(tempBottom);
                tempTop.toArray(top);
                tempBottom.toArray(bottom);
            }

            public static int[] getElement(int value){

                        switch (value){
                        case 0:
                                return new int[]{0,1};
                        case 1:
                                return new int[]{2,2};
                        case 2:
                                return new int[]{3,4};
                        case 3:
                                return new int[]{2,4};
                        case 4:
                                return new int[]{0,3};
                        case 5:
                                return new int[]{1,2};
                        case 6:
                                return new int[]{3,1};
                        case 7:
                                return new int[]{2,0};
                        case 8:
                                return new int[]{1,3};
                        case 9:
                                return new int[]{0,4};
                        case 10:
                                return new int[]{0,0};
                        case 11:
                                return new int[]{3,3};
                        case 12:
                                return new int[]{2,1};
                        case 13:
                                return new int[]{0,2};
                        case 14:
                                return new int[]{2,3};
                        case 15:
                                return new int[]{3,2};
                        case 16:
                                return new int[]{1,1};
                        case 17:
                                return new int[]{1,0};
                        case 18:
                                return new int[]{1,4};
                        case 19:
                                return new int[]{3,0};
                        default:
                                return new int[]{0,0};


                            }
            }
}
