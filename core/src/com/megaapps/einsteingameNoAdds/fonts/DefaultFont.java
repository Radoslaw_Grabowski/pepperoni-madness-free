package com.megaapps.einsteingameNoAdds.fonts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by k on 25.04.2017.
 */

public class DefaultFont
{
	private BitmapFont font;
	private int size;

	public DefaultFont( int size )
	{
		this.size = size;
		initFont();
	}

	public void setSize( int size )
	{
		this.size = size;
	}

	private void initFont()
	{
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator( Gdx.files.internal( "styles/gameFont.ttf" ) );
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = size;
		font = generator.generateFont( parameter );
		generator.dispose();

	}

	public BitmapFont getFont()
	{
		return font;
	}

	public void setColor( Color color )
	{
		font.setColor( color );
	}
}
