package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;

/**
 * Created by lukasz on 28.04.17.
 */

public class Element {

    private int colorValue;
    Element() {
        this(0);
    }

    Element(int value) {
        this.colorValue = value;
    }

    public int getColorValue() {
        return colorValue;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (other instanceof Element) {
            Element otherElement = (Element) other;
            return this.colorValue == otherElement.colorValue;
        } else {
            return false;
        }
    }

    public void setColorValue(int colorValue) {
        this.colorValue = colorValue;
    }
}