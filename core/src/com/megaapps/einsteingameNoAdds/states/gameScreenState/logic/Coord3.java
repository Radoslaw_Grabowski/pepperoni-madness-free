package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;

/**
 * Created by lukasz on 05.05.17.
 */
public class Coord3 {
    private int x, y, z;

    public Coord3(int x, int  y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }

    public int metricDist(Coord3 coord) {
        return Math.abs(coord.getX() - this.x) +
                Math.abs(coord.getY() - this.y) +
                Math.abs(coord.getZ() - this.z);
    }

    public double dist(Coord3 coord) {
        return Math.sqrt((this.x - coord.getX())*(this.x - coord.getX()) +
                (this.y - coord.getY())*(this.y - coord.getY()) +
                (this.z - coord.getZ())*(this.z - coord.getZ()));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Coord3)) return false;
        if (o == this) return true;
        return ((Coord3) o).getX() == x && ((Coord3) o).getY() == y && ((Coord3) o).getZ() == z;
    }
}