package com.megaapps.einsteingameNoAdds.states.gameScreenState;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.Game;
import com.megaapps.einsteingameNoAdds.GameStateManager;
import com.megaapps.einsteingameNoAdds.helpers.MusicHelper;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.State;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.board.GameStage;

import static com.megaapps.einsteingameNoAdds.Constants.DEFAULT_CLEAR_COLORS;

/**
 * Created by lukasz on 27.04.17.
 */

public class GameScreenState extends State
{
	private Stage bottomTabStage;
	private GameStage gameStage;
	//private OverlayStage overlayStage;

	public GameScreenState( GameStateManager gsm, NewAssetManager as )
	{
		super( gsm, as );
		Game.bus.register( this );
	}

	public void ResetGame()
	{
		gameStage.resetGame();
	}

	@Override
	public void initialize()
	{
		bottomTabStage = new Stage( new FillViewport( Constants.GAME_SIZE_WIDTH, com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_HEIGHT ) );
		gameStage = new GameStage( gsm, assetManager );


		Image bottomTab = new Image( assetManager.get( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BOTTOM_LABEL_PATH, Texture.class ) );
		bottomTab.setPosition( 0, 0 );
		bottomTab.setSize( com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_WIDTH, com.megaapps.einsteingameNoAdds.Constants.BOTTOM_TAB_HEIGHT );
		bottomTabStage.addActor( bottomTab );

		Image blackLabel = new Image( assetManager.get( Constants.GAME_SCREEN_STATE_SINGLE_MINI_GAME_BLACK_LABEL_PATH, Texture.class ) );
		blackLabel.setSize( com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_WIDTH, com.megaapps.einsteingameNoAdds.Constants.MINIGAME_BOTTOM_LABEL_BLACK_HEIGHT );
		blackLabel.setPosition( 0, com.megaapps.einsteingameNoAdds.Constants.MINIGAME_BOTTOM_LABEL_BLACK );
		bottomTabStage.addActor( blackLabel );

		Gdx.gl.glClearColor( DEFAULT_CLEAR_COLORS.x, DEFAULT_CLEAR_COLORS.y, DEFAULT_CLEAR_COLORS.z, 1 );

		isInitialized = true;
	}

	@Override
	public void loadAssets()
	{

	}

	@Override
	public void pause( boolean pauseMusic )
	{
//        PreferencesHelper.saveLastTimeout(System.currentTimeMillis() / 1000);
		com.megaapps.einsteingameNoAdds.helpers.MusicHelper.getThemeMusic().pause();
	}

	@Override
	public void update( float dt )
	{
		bottomTabStage.act( dt );
		gameStage.act( dt );
		//	overlayStage.act( dt );
	}

	@Override
	public void render( SpriteBatch sb )
	{
		bottomTabStage.draw();
		gameStage.draw();
		//	overlayStage.draw();
	}

	@Override
	public void dispose()
	{
		gameStage.dispose();
		bottomTabStage.dispose();
		//	overlayStage.dispose();
		Game.bus.unregister( this );
//		if( getMiniGameMusic().isPlaying() )
//			getMiniGameMusic().pause();
	}

	@Override
	public void resize( int width, int height )
	{
		bottomTabStage.getViewport().update( width, height );
		gameStage.resize( width, height );
		//	overlayStage.resize( width, height );

	}

	@Override
	public void resume()
	{
		if( !PreferencesHelper.getMusicMute() )
			MusicHelper.getThemeMusic().play();
	}

	public void invalidateGame()
	{
		if( gameStage != null )
		{
			gameStage.invalidateHighScore();
		}
	}

	@Override
	public void setInputs()
	{
		gameStage.setInputs();
	}

}
