package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.megaapps.einsteingameNoAdds.Constants;

import static com.megaapps.einsteingameNoAdds.Constants.MODIFIER_SIZE;

/**
 * Created by Kamil on 6/13/2017.
 */

public class DraggableEraser extends Image implements Draggable {
    private boolean isDragged;
    private Rectangle boundingRectangle, slotRectangle;
    ;
    private Vector2 startPosition;

    public DraggableEraser(Texture texture, float x, float y) {
        super(texture);
        setSize(MODIFIER_SIZE, MODIFIER_SIZE);
        setPosition(x, y);
        startPosition = new Vector2(getX(), getY());
        boundingRectangle = new Rectangle(getX(), getY(), getWidth() * 1.5f, getHeight());
        slotRectangle = new Rectangle(getX() + getWidth() / 2 - boundingRectangle.getWidth() / 4, getY() + getHeight() / 2 - boundingRectangle.getHeight() / 4, boundingRectangle.getWidth() / 2, boundingRectangle.getHeight() / 2);
                boundingRectangle.setPosition(getX() + MODIFIER_SIZE / 2 - boundingRectangle.getWidth() / 2,
                                getY() + MODIFIER_SIZE / 2 - boundingRectangle.getHeight() / 2);
                slotRectangle.setPosition(getX() + getWidth() / 2 - slotRectangle.getWidth() / 2, getY() + getHeight() / 2 - slotRectangle.getHeight() / 2);
    }

    @Override
    public boolean isDragged() {
        return isDragged;
    }

    @Override
    public void setDragged(boolean dragged) {
        this.isDragged = dragged;
    }

    @Override
    public void dragImage(float x, float y) {
        setPosition(x - getWidth() / 2, y - getHeight() / 2);
        boundingRectangle.setPosition(getX() + MODIFIER_SIZE / 2 - boundingRectangle.getWidth() / 2,
                getY() + MODIFIER_SIZE / 2 - boundingRectangle.getHeight() / 2);
        slotRectangle.setPosition(getX() + getWidth() / 2 - slotRectangle.getWidth() / 2, getY() + getHeight() / 2 - slotRectangle.getHeight() / 2);
    }

    @Override
    public Rectangle getBounds() {
        return boundingRectangle;
    }

    @Override
    public Rectangle getSlotRectangle() {
        return slotRectangle;
    }

    @Override
    public Vector2 getStartPosition() {
        return startPosition;
    }

    @Override
    public void returnToStartPosition() {
        MoveToAction moveToAction = new MoveToAction();
        moveToAction.setPosition(getStartPosition().x, getStartPosition().y);
        boundingRectangle.setPosition(getStartPosition().x + MODIFIER_SIZE / 2 - boundingRectangle.getWidth() / 2,
                getStartPosition().y + MODIFIER_SIZE / 2 - boundingRectangle.getHeight() / 2);
        slotRectangle.setPosition(getStartPosition().x + getWidth() / 2 - slotRectangle.getWidth() / 2, getStartPosition().y + getHeight() / 2 - slotRectangle.getHeight() / 2);
        moveToAction.setDuration(0.5f);
        addAction(moveToAction);
    }

    @Override
    public void setToStartPosition() {
        setPosition(startPosition.x, startPosition.y);
        boundingRectangle.setPosition(getX() + MODIFIER_SIZE / 2 - boundingRectangle.getWidth() / 2,
                getY() + MODIFIER_SIZE / 2 - boundingRectangle.getHeight() / 2);
        slotRectangle.setPosition(getX() + getWidth() / 2 - slotRectangle.getWidth() / 2, getY() + getHeight() / 2 - slotRectangle.getHeight() / 2);
    }
}
