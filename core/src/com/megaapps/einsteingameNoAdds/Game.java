package com.megaapps.einsteingameNoAdds;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.interfaces.AddsInterface;
import com.megaapps.einsteingameNoAdds.interfaces.HomeButtonInterface;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.LoadingScreen;
import com.megaapps.einsteingameNoAdds.states.gameOnlyStates.GameOnlyMainScreenState;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.GameScreenState;
import com.megaapps.einsteingameNoAdds.states.tutorialScreenState.TutorialState;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;


public class Game extends ApplicationAdapter
{
	public static Bus bus = new Bus( ThreadEnforcer.ANY );
	public static AddsInterface addsInterface;
	public static HomeButtonInterface homeButtonInterface;
	private SpriteBatch batch;
	private GameStateManager gameStateManager;
	private NewAssetManager assetManager;
	private LoadingScreen loadingScreen;

	public Game( AddsInterface addsInterface, HomeButtonInterface homeButtonInterface )
	{
		this.addsInterface = addsInterface;
		this.homeButtonInterface = homeButtonInterface;
	}

	@Override
	public void create()
	{
		Gdx.gl.glClearColor( Constants.DEFAULT_CLEAR_COLORS.x, Constants.DEFAULT_CLEAR_COLORS.y, Constants.DEFAULT_CLEAR_COLORS.z, 1 );
		assetManager = new NewAssetManager();
		batch = new SpriteBatch();
		loadingScreen = new LoadingScreen( batch, assetManager );
		gameStateManager = new GameStateManager( assetManager, loadingScreen );

		if( PreferencesHelper.getFirstTime() || Constants.ALWAYS_SHOW_TUTORIAL )
		{
			gameStateManager.add( new TutorialState( gameStateManager, assetManager ) );
		}
		else
		{
			GameScreenState m_gameScreenState = new GameScreenState( gameStateManager, assetManager);
			gameStateManager.add( new GameOnlyMainScreenState( gameStateManager, assetManager, m_gameScreenState ) );
		}

		addsInterface.loadAds();
	}

	private void clearPreferences()
	{
		PreferencesHelper.saveLevel( 1 );
		PreferencesHelper.saveBooks( 0 );
		PreferencesHelper.saveProgress( 0 );
		PreferencesHelper.saveUpgradePoints( 2 );
		PreferencesHelper.saveMoney( 30 );
	}

	@Override
	public void render()
	{
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );

		gameStateManager.render( Gdx.graphics.getDeltaTime(), batch );
	}

	@Override
	public void dispose()
	{
		batch.dispose();
	}

	@Override
	public void resume()
	{
		super.resume();
		assetManager.resume();
		gameStateManager.resume();
	}

	@Override
	public void pause()
	{
		super.pause();
		gameStateManager.pause();
	}

	@Override
	public void resize( int width, int height )
	{
		super.resize( width, height );
		gameStateManager.resize( width, height );
	}
}
