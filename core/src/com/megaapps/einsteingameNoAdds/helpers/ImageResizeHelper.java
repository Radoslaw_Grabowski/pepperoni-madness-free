package com.megaapps.einsteingameNoAdds.helpers;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by lukasz on 11.05.17.
 */

public class ImageResizeHelper {

    public static void setWidthAndChangeHeightProportionally(Image image, float width) {
        float heightToWidthRatio = image.getHeight() / image.getWidth();
        image.setWidth(width);
        image.setHeight(heightToWidthRatio * width);
    }

    public static void setHeightAndChangeWidthProportionally(Image image, float height) {
        float widthToHeightRatio = image.getWidth() / image.getHeight();
        image.setHeight(height);
        image.setWidth(widthToHeightRatio * height);
    }

}
