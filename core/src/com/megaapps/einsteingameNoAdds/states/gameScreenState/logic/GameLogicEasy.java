package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;

import com.megaapps.einsteingameNoAdds.events.AddModificatorsPoints;
import com.megaapps.einsteingameNoAdds.interfaces.AddsInterface;
import com.megaapps.einsteingameNoAdds.exceptions.GameOverException;
import com.megaapps.einsteingameNoAdds.helpers.MusicHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.megaapps.einsteingameNoAdds.Game.bus;

/**
 * Created by lukasz on 04.05.17.
 */


public class GameLogicEasy extends GameLogic {

    private static final String TAG = GameLogicMedium.class.getSimpleName();

    Set<Diagonal> diagonals = new HashSet<Diagonal>();

    private double DIST_1 = new Coord3(0, 0, 0).dist(new Coord3(0, 0, 2));
    private double DIST_2 = new Coord3(0, 0, 0).dist(new Coord3(0, 2, 2));

    GameLogicEasy() {
        super();
        setup();
    }

    private void setup() {
        List<Coord3> tmp = new ArrayList<Coord3>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!(i == 1 && j == 1)) {
                    tmp.add(new Coord3(i, j, 0));
                }
            }
        }
        for (int i =  0; i < tmp.size(); i++) {
            for(int j = i+1; j < tmp.size(); j++) {
                Coord3 coord1 = tmp.get(i);
                Coord3 coord2 = tmp.get(j);
                double dist = coord1.dist(coord2);
                if (dist == DIST_1 || dist == DIST_2) {
                    diagonals.add(new Diagonal(coord1, coord2));
                }
            }
        }
    }




    @Override
    public List<Coord3> update() throws GameOverException{
        List<Coord3> toRemove = new ArrayList<Coord3>();
        colorTypeLoop: for (int type = 0; type < NUMBER_OF_ELEMENT_TYPES; type++) {
            Element elem = new Element(type);
            // look for 3 stacked on top
            for (int i = 0; i < 3; i++) {
                for(int j = 0; j < 3; j++) {
                    if (elem.equals(board[i][j][0]) &&
                        elem.equals(board[i][j][1]) &&
                        elem.equals(board[i][j][2])) {
                        for (int x = 0; x < 3; x++) {
                            for(int y = 0; y < 3; y++) {
                                for (int z = 0; z < 3; z++) {
                                    if (elem.equals(board[x][y][z])) {
                                        toRemove.add(new Coord3(x, y, z));
                                    }
                                }
                            }
                        }
                        break colorTypeLoop;
                    }
                }
            }
            // check diagonals if no stacked elements found
            for (Diagonal diagonal : diagonals) {
                Coord3[] coords = diagonal.getDiagonalCoords();
                if (
                        (elem.equals(board[coords[0].getX()][coords[0].getY()][0]) ||
                         elem.equals(board[coords[0].getX()][coords[0].getY()][1]) ||
                         elem.equals(board[coords[0].getX()][coords[0].getY()][2]))
                        &&
                        (elem.equals(board[coords[1].getX()][coords[1].getY()][0]) ||
                         elem.equals(board[coords[1].getX()][coords[1].getY()][1]) ||
                         elem.equals(board[coords[1].getX()][coords[1].getY()][2]))
                        &&
                        (elem.equals(board[coords[2].getX()][coords[2].getY()][0]) ||
                         elem.equals(board[coords[2].getX()][coords[2].getY()][1]) ||
                         elem.equals(board[coords[2].getX()][coords[2].getY()][2]))
                        ) {
                    for (int i = 0; i < coords.length; i++) {
                        for(int j = 0; j < 3; j++) {
                            if (new Element(type).equals(board[coords[i].getX()][coords[i].getY()][j])) {
                                toRemove.add(new Coord3(coords[i].getX(), coords[i].getY(), j));
                            }
                        }
                    }
                }
            }
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j][0] != null && board[i][j][1] != null && board[i][j][2] != null &&
                    board[i][j][0].equals(board[i][j][1]) && board[i][j][1].equals(board[i][j][2])){
                    toRemove.add(new Coord3(i,j,0));
                    toRemove.add(new Coord3(i,j,1));
                    toRemove.add(new Coord3(i,j,2));
                }
            }
        }
        score += (toRemove.size() * PER_SUCCESS_POINTS);
        increaseElementColorRange(score);
        popElements+=toRemove.size();
        if(toRemove.size()>0){
            combo++;
            MusicHelper.removePizzaSound();
            showCombo();
        }else
            combo=0;

        for (Coord3 coord : toRemove) {
            board[coord.getX()][coord.getY()][coord.getZ()] = null;
        }
        currentElements = super.generateCurrentElements();
        return toRemove;
    }


    private class Diagonal {

        private Coord3 corner1, corner2;

        public Diagonal(Coord3 corner1, Coord3 corner2) {
            this.corner1 = corner1;
            this.corner2 = corner2;
        }

        public Coord3[] getDiagonalCoords() {
            Coord3[] coords = new Coord3[DIMENSIONALITY];
            coords[0] = corner1;
            coords[1] = new Coord3((corner1.getX() + corner2.getX()) / 2,
                                   (corner1.getY() + corner2.getY()) / 2,
                                   (corner1.getZ() + corner2.getZ()) / 2);
            coords[2] = corner2;
            return coords;
        }

        @Override
        public String toString() {
            return "(" + corner1.toString() + ", " + corner2.toString() + ")";
        }
    }
}