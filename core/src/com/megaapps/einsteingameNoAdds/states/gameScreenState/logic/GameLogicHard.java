package com.megaapps.einsteingameNoAdds.states.gameScreenState.logic;

import com.badlogic.gdx.Gdx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by lukasz on 27.04.17.
 */

public class GameLogicHard extends GameLogic {

    private static final String TAG = GameLogicHard.class.getSimpleName();


    private Set<Diagonal> diagonals = new HashSet<Diagonal>();

    private double DIST_1 = new Coord3(0, 0, 0).dist(new Coord3(0, 0, 2));
    private double DIST_2 = new Coord3(0, 0, 0).dist(new Coord3(0, 2, 2));
    private double DIST_3 = new Coord3(0, 0, 0).dist(new Coord3(2, 2, 2));


    public GameLogicHard() {
        super();
        setup();
    }


    private void setup() {
        List<Coord3> tmp = new ArrayList<Coord3>();
        for (int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    if (!(i == 1 && j == 1 && k == 1))
                        tmp.add(new Coord3(i, j, k));
                }
            }
        }
        for (int i = 0; i < tmp.size()-1; i++) {
            for (int j = i+1; j < tmp.size(); j++) {
                Coord3 coord1 = tmp.get(i);
                Coord3 coord2 = tmp.get(j);
                double dist = coord1.dist(coord2);
                if (dist == DIST_1 || dist == DIST_2 || dist == DIST_3) {
                    diagonals.add(new Diagonal(coord1, coord2));
                }
            }
        }
    }


    @Override
    public List<Coord3> update() throws com.megaapps.einsteingameNoAdds.exceptions.GameOverException {
        List<Diagonal> diagonalsToRemove = new ArrayList<Diagonal>();
        List<Coord3> toRemove = new ArrayList<Coord3>();
        int combo_count = 0;
        for (Diagonal diagonal : diagonals) {
            Coord3[] coords = diagonal.getDiagonalCoords();
            if (board[coords[0].getX()][coords[0].getY()][coords[0].getZ()] != null &&
                    board[coords[1].getX()][coords[1].getY()][coords[1].getZ()] != null &&
                    board[coords[2].getX()][coords[2].getY()][coords[2].getZ()] != null &&
                    board[coords[0].getX()][coords[0].getY()][coords[0].getZ()].getColorValue() ==
                            board[coords[1].getX()][coords[1].getY()][coords[1].getZ()].getColorValue() &&
                    board[coords[1].getX()][coords[1].getY()][coords[1].getZ()].getColorValue() ==
                            board[coords[2].getX()][coords[2].getY()][coords[2].getZ()].getColorValue()) {
                // 3 values in a row
                diagonalsToRemove.add(diagonal);
                toRemove.addAll(Arrays.asList(coords));
                score += PER_SUCCESS_POINTS;
                popElements+=1;
                combo_count++;
                Gdx.app.log(TAG, "combo_count: " + combo_count);
            }
        }
        for (Diagonal diagonal: diagonalsToRemove) {
            Coord3[] coords = diagonal.getDiagonalCoords();
            board[coords[0].getX()][coords[0].getY()][coords[0].getZ()] = null;
            board[coords[1].getX()][coords[1].getY()][coords[1].getZ()] = null;
            board[coords[2].getX()][coords[2].getY()][coords[2].getZ()] = null;
        }
        diagonalsToRemove.clear();
        currentElements = super.generateCurrentElements();
        return toRemove;
    }

    private class Diagonal {

        private Coord3 corner1, corner2;

        public Diagonal(Coord3 corner1, Coord3 corner2) {
            this.corner1 = corner1;
            this.corner2 = corner2;
        }

        public Coord3[] getDiagonalCoords() {
            Coord3[] coords = new Coord3[DIMENSIONALITY];
            coords[0] = corner1;
            coords[1] = new Coord3((corner1.getX() + corner2.getX()) / 2,
                    (corner1.getY() + corner2.getY()) / 2,
                    (corner1.getZ() + corner2.getZ()) / 2);
            coords[2] = corner2;
            return coords;
        }

        @Override
        public String toString() {
            return "(" + corner1.toString() + ", " + corner2.toString() + ")";
        }
    }

}
