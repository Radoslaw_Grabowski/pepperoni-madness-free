package com.megaapps.einsteingameNoAdds.events;

/**
 * Created by Kamil on 6/14/2017.
 */

public class AddModificatorsPoints {

    com.megaapps.einsteingameNoAdds.interfaces.AddsInterface.ModificatorType type;

    public AddModificatorsPoints(com.megaapps.einsteingameNoAdds.interfaces.AddsInterface.ModificatorType type) {
        this.type = type;
    }

    public com.megaapps.einsteingameNoAdds.interfaces.AddsInterface.ModificatorType getType() {
        return type;
    }
}
