package com.megaapps.einsteingameNoAdds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;
import com.megaapps.einsteingameNoAdds.events.AddModificatorsPoints;
import com.megaapps.einsteingameNoAdds.helpers.MusicHelper;
import com.megaapps.einsteingameNoAdds.helpers.PreferencesHelper;
import com.megaapps.einsteingameNoAdds.interfaces.AddsInterface;
import com.megaapps.einsteingameNoAdds.interfaces.HomeButtonInterface;

import org.robovm.apple.coregraphics.CGRect;
import org.robovm.apple.coregraphics.CGSize;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSErrorException;
import org.robovm.apple.foundation.NSURL;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIApplicationLaunchOptions;
import org.robovm.apple.uikit.UIApplicationOpenURLOptions;
import org.robovm.pods.chartboost.CBLoadError;
import org.robovm.pods.chartboost.CBLocation;
import org.robovm.pods.chartboost.Chartboost;
import org.robovm.pods.chartboost.ChartboostDelegateAdapter;
import org.robovm.pods.google.GGLContext;
import org.robovm.pods.google.mobileads.GADAdSize;
import org.robovm.pods.google.mobileads.GADBannerView;
import org.robovm.pods.google.mobileads.GADBannerViewDelegateAdapter;
import org.robovm.pods.google.mobileads.GADRequest;
import org.robovm.pods.google.mobileads.GADRequestError;

import java.util.ArrayList;
import java.util.List;

import static com.megaapps.einsteingameNoAdds.Game.bus;


public class IOSLauncher extends IOSApplication.Delegate implements AddsInterface, HomeButtonInterface
{

	private static final String TAG = "iOSAdds";
	private final static String IOS_AD_MOB_BANNER_ID = "ca-app-pub-1157452047737947/8142200718";
	private final static String IOS_AD_MOB_REQUEST_VIDEO_ID = "ca-app-pub-1157452047737947/9618933916";

	private GADRequest request;
	private GADBannerView adview;
	private ModificatorType type = AddsInterface.ModificatorType.ERASER;

	@Override
	protected IOSApplication createApplication() {
		IOSApplicationConfiguration config = new IOSApplicationConfiguration();
		config.orientationLandscape = false;
		config.orientationPortrait = true;
		bus.register(this);
		return new IOSApplication(new Game(this,this), config);
	}

	@Override
	public boolean didFinishLaunching(UIApplication application, UIApplicationLaunchOptions launchOptions) {
		return super.didFinishLaunching(application, launchOptions);
	}

	public static void main(String[] argv) {

		NSAutoreleasePool pool = new NSAutoreleasePool();
		UIApplication.main(argv, null, IOSLauncher.class);
		pool.close();
	}

	@Override
	public void loadAds() {


		String appId= "59a66b974744690c0af941f6";
		String appSignature ="0a3095485ca5f66be7a55f92887156bdef628f06";


		Chartboost.start(appId, appSignature, new ChartboostDelegateAdapter() {
			@Override
			public void didInitialize(boolean status) {
				Gdx.app.log("initialize","initialize");

			}

			@Override
			public void didCloseInterstitial( String location )
			{
				Chartboost.cacheInterstitial( CBLocation.MainMenu );
				super.didCloseInterstitial( location );
			}

			@Override
			public boolean shouldDisplayRewardedVideo(String location) {

				return true;
			}

			@Override
			public void didDisplayRewardedVideo(String location) {
				if (MusicHelper.getThemeMusic().isPlaying())
					MusicHelper.getThemeMusic().pause();
			}

			@Override
			public void didCacheRewardedVideo(String location) {
				Gdx.app.log("didCacheRewardedVideo","didCacheRewardedVideo "+location);
			}

			@Override
			public void didFailToLoadRewardedVideo(String location, CBLoadError error) {
				Gdx.app.log("didFailToLoadRewardedVideo","didFailToLoadRewardedVideo "+location);

			}

			@Override
			public void didDismissRewardedVideo(String location) {
				Gdx.app.log("didDismissRewardedVideo","didDismissRewardedVideo "+location);
			}

			@Override
			public void didCloseRewardedVideo(String location) {
				if (!PreferencesHelper.getMusicMute())
					MusicHelper.getThemeMusic().play();

				Gdx.app.log("didCloseRewardedVideo","didCloseRewardedVideo "+location);
			}

			@Override
			public void didClickRewardedVideo(String location) {
				Gdx.app.log("didClickRewardedVideo","didClickRewardedVideo "+location);
			}

			@Override
			public void didCompleteRewardedVideo(String location, int reward) {

				bus.post(new AddModificatorsPoints(type));
				Gdx.app.log("didCompleteRewardedVideo","didCompleteRewardedVideo "+location);
			}

		});

		Chartboost.cacheRewardedVideo(CBLocation.MainMenu);
		Chartboost.cacheInterstitial(CBLocation.MainMenu);

		adview = new GADBannerView(GADAdSize.Banner());
		adview.setAdUnitID(IOS_AD_MOB_BANNER_ID); //put your banner key here
		adview.setRootViewController(((IOSApplication) Gdx.app).getUIViewController());
		((IOSApplication) Gdx.app).getUIViewController().getView().addSubview(adview);

		request = new GADRequest();
		String testDevice = request.getSimulatorID();
		List array = new ArrayList();
		array.add(testDevice);
		array.add("5ec2813594eb4b414885b2fefe42a62b");
		array.add("38790A32-4D53-5CFC-B742-84FFA710DD0E");
		array.add("4b3296b07f864d54bfa17cc9fad1c607");
		request.setTestDevices(array);


		adview.setDelegate(new GADBannerViewDelegateAdapter() {
			@Override
			public void didReceiveAd(GADBannerView view) {
				super.didReceiveAd(view);
				Gdx.app.log(TAG, "didReceiveBannerAd");
			}

			@Override
			public void didFailToReceiveAd(GADBannerView view,
										   GADRequestError error) {
				super.didFailToReceiveAd(view, error);

			}
		});

		adview.loadRequest(request);

	}

	@Override
	public void showInterstitialAdd()
	{
		Chartboost.showInterstitial(CBLocation.MainMenu);
	}

	@Override
	public void showBannerAds(boolean show) {
		double screenWidth = Gdx.graphics.getWidth();
		final CGSize adSize = adview.getBounds().getSize();
		double adWidth = adSize.getWidth();
		double adHeight = adSize.getHeight();


		float bannerWidth = (float) screenWidth;
		float bannerHeight = (float) (bannerWidth / adWidth * adHeight);


		if (show) {
			adview.setFrame(new CGRect((screenWidth / 2) - adWidth / 2, 0, bannerWidth, bannerHeight));
			Gdx.app.log(TAG, "Add showed");

		} else {

			Gdx.app.log(TAG, "Add hide");
		}

	}

	@Override
	public void showRequestVideoAdd(boolean show, AddsInterface.ModificatorType type) {
		this.type = type;
		Chartboost.showRewardedVideo(CBLocation.MainMenu);
		Gdx.app.log("kamil", "showRequestVideoAdd");
	}

	@Override
	public void homeButtonClicked() {

	}

}