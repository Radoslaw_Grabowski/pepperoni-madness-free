package com.megaapps.einsteingameNoAdds.events;

/**
 * Created by k on 26.05.2017.
 */

public class ShowComboEvent {

    private int combo;

    public ShowComboEvent(int combo) {
        this.combo = combo;
    }

    public int getCombo() {
        return combo;
    }
}
