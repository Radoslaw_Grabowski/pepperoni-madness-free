package com.megaapps.einsteingameNoAdds.states.gameScreenState.board;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.megaapps.einsteingameNoAdds.Constants;
import com.megaapps.einsteingameNoAdds.exceptions.InterceptFailedException;
import com.megaapps.einsteingameNoAdds.managers.NewAssetManager;
import com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.GameLogic;

import static com.megaapps.einsteingameNoAdds.Constants.GAME_SIZE_HEIGHT;


/**
 * Created by lukasz on 02.05.17.
 */

public class GameBoard extends Table
{

	private static float DOWN_OFFSET = GAME_SIZE_HEIGHT * .35f;
	private float HORIZONTAL_OFFSET;
	private Group[][] fields;
	private Rectangle[][] slotBounds;
	private GameLogic gameLogic;
	private TextureAtlas atlas;

	public GameBoard( GameLogic gameLogic, NewAssetManager assetManager )
	{
		atlas = assetManager.get( "gameScreenState/singleMiniGame/elements.atlas", TextureAtlas.class );
		this.gameLogic = gameLogic;
		setSize( Constants.GAME_SIZE_WIDTH, Constants.GAME_SIZE_WIDTH + 4 * Constants.GAME_BOARD_PADDING );
		fields = new Group[3][3];
		slotBounds = new Rectangle[3][3];
		initEmptyBoard();
		align( Align.center ).pack();

		HORIZONTAL_OFFSET = ( Constants.GAME_SIZE_WIDTH - getWidth() ) / 2;
		setPosition( HORIZONTAL_OFFSET, DOWN_OFFSET );

		for( int i = 0; i < fields.length; ++i )
		{
			for( int j = 0; j < fields[0].length; ++j )
			{
				slotBounds[i][j] = new Rectangle( HORIZONTAL_OFFSET + fields[i][j].getX() + fields[i][j].getWidth() / 2 - fields[i][j].getWidth() / 3f, DOWN_OFFSET + fields[i][j].getY() + fields[i][j].getHeight() / 2 - fields[i][j].getHeight() / 3f,
												  fields[i][j].getWidth() / 1.5f, fields[i][j].getHeight() / 1.5f );
			}
		}

	}

	public float getHORIZONTAL_OFFSET()
	{
		return HORIZONTAL_OFFSET;
	}

	public Rectangle[][] getSlotBounds()
	{
		return slotBounds;
	}

	public Group[][] getFields()
	{
		return fields;
	}

	private void initEmptyBoard()
	{
		for( int i = 0; i < fields.length; ++i )
		{
			for( int j = 0; j < fields[0].length; ++j )
			{

				Group group = new Group();
				Image slotImage = new Image( atlas.findRegion( "frame" ) );
			  /*  Color tintingColor= new Color(Color.GREEN);
                slotImage.setColor(Color.WHITE.cpy().lerp(tintingColor, 1f)); todo:: change colors*/
				slotImage.setSize( Constants.ELEMENT_SIZE, Constants.ELEMENT_SIZE );
				group.addActor( slotImage );
				fields[i][j] = group;
				add( fields[i][j] ).size( Constants.ELEMENT_SIZE ).pad( Constants.GAME_BOARD_PADDING );
			}
			row();
		}
	}

 /*   private void clearBoard() {
        for (int i = 0; i < fields.length; ++i) {
            for (int j = 0; j < fields[0].length; ++j) {
                    fields[i][j].clearChildren();
                Group group = new Group();
                Image backgroundImage = new Image(new Texture("gameScreenState/slot.png"));
                backgroundImage.setSize(fields[i][j].getWidth(), fields[i][j].getHeight());
                group.addActor(backgroundImage);
                fields[i][j].addActor(group);
            }
        }
    }*/

	public com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord2 ifIntercept(
			Draggable item )
			throws InterceptFailedException
	{

		if( item instanceof DraggableEraser )
		{
			for( int i = 0; i < slotBounds.length; ++i )
			{
				for( int j = 0; j < slotBounds[0].length; ++j )
				{
					if( item.getSlotRectangle().overlaps( slotBounds[i][j] ) )
					{

						if( gameLogic.checkIfEmpty( i, j ) )
							throw new InterceptFailedException(); // case when the field is empty
						else
						{
							if( gameLogic.decrementErases() )
								return new com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord2( i, j );
						}
					}
				}
			}
		}
		else
		{
			for( int i = 0; i < slotBounds.length; ++i )
			{
				for( int j = 0; j < slotBounds[0].length; ++j )
				{
					if( item.getSlotRectangle().overlaps( slotBounds[i][j] ) && gameLogic.setValue( i, j ) )
					{
						return new com.megaapps.einsteingameNoAdds.states.gameScreenState.logic.Coord2( i, j );
					}
				}
			}
		}
		throw new InterceptFailedException();
	}

	public float getDownOffset()
	{
		return DOWN_OFFSET;
	}


}
